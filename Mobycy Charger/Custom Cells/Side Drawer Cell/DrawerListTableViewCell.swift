//
//  DrawerListTableViewCell.swift
//  Mobycy Charger
//
//  Created by sarvesh singh on 1/21/20.
//  Copyright © 2020 Zypp. All rights reserved.
//

import UIKit

class DrawerListTableViewCell: UITableViewCell {

    @IBOutlet weak var drawerOptionImageView: UIImageView!
    @IBOutlet weak var drawerOptionLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cellDesignLayout()
    }
    
    //TODO :- func to set cell design layout
    func cellDesignLayout(){
        drawerOptionLbl.textColor = customColor.lightBlackColor
        drawerOptionLbl.font = UIFont(name: TextFontStyle.fontRegular, size: CGFloat(FontSize.headingFont14))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
