//
//  Utility.swift
//  Mobycy Charger
//
//  Created by sarvesh singh on 1/14/20.
//  Copyright © 2020 Zypp. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class Utility: NSObject {
    
    /*
     TODO :- Check staging and production url
     Parameters :- Get Value for return api url //0 for staging and 1 for production
     Return : return staging/production url
     */
    static func returnBaseURL(liveURL : Int) -> String {
        if (liveURL == 0) {
            return "http://13.126.228.36/mobycy/api/";
        }
        else if (liveURL == 1){
            return "http://13.126.16.169/mobycy/api/";
        }
        else{
            return "https://220901b3.ngrok.io/mobycy/api/";
        }
    }
    
    /**
     This method used to show alert message
     Parameters :-  title for alert message
     message to show alert message
     */
    static func showAlertView(_ message:String,title:String){
        DispatchQueue.main.async { () -> Void in
            let finalString2 = message.inserting(separator: " \n ", every: 35)
            
            let alert:UIAlertController = UIAlertController.init(title: title, message: finalString2, preferredStyle:.alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(defaultAction)
            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    /**
     This method used for showing alert with performing action
     - Parameters:
     - message: This method take the alert message in string format
     - title: This method take the alert message in string format
     - onView: This method take the viewcontroller for ditermining where we have to show alert
     */
    
    static func showSingleBntAlertControllerAction(message:String,title:String, onView controller:UIViewController, andFirstButtonTitle firstButtonTitle:String, andFirstAction firstAction:Selector!){
        
        DispatchQueue.main.async() { () -> Void in
            
            let finalString2 = message.inserting(separator: " \n ", every: 35)
            let alert:UIAlertController = UIAlertController.init(title: title, message: finalString2, preferredStyle: UIAlertController.Style.alert)
            
            let yesAction = UIAlertAction(title:firstButtonTitle, style: .default, handler: {(alert:UIAlertAction) -> Void in
                
                if firstAction != nil{
                    controller.perform(firstAction)
                }
            })
            
            alert.addAction(yesAction)
            controller.present(alert, animated: true, completion: nil)
        }
    }
    
    /**
     This method used for showing alert with performing action
     - Parameters:
     - message: This method take the alert message in string format
     - title: This method take the alert message in string format
     - onView: This method take the viewcontroller for ditermining where we have to show alert
     */
    
    static func showAlertControllerAction(message:String,title:String, onView controller:UIViewController, andFirstButtonTitle firstButtonTitle:String,andSecondButtonTitle secondButtonTitle:String, andFirstAction firstAction:Selector!, andSecondAction secondAction:Selector!){
        
        DispatchQueue.main.async() { () -> Void in
            let alert:UIAlertController = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            
            let defaultAction = UIAlertAction(title:firstButtonTitle, style: .default, handler:{(alert:UIAlertAction) -> Void in
                
                if firstAction != nil{
                    controller.perform(firstAction)
                }
            })
            
            let yesAction = UIAlertAction(title:secondButtonTitle, style: .default, handler: {(alert:UIAlertAction) -> Void in
                
                if secondAction != nil{
                    controller.perform(secondAction)
                }
            })
            
            alert.addAction(defaultAction)
            alert.addAction(yesAction)
            controller.present(alert, animated: true, completion: nil)
        }
    }
    
    /**
     This method used for showing alert with performing action
     - Parameters:
     - message: This method take the alert message in string format
     - title: This method take the alert message in string format
     - onView: This method take the viewcontroller for ditermining where we have to show alert
     */
    
    static func showAlertControllerOneButtonAction(message:String,title:String, onView controller:UIViewController, andFirstButtonTitle firstButtonTitle:String, andFirstAction firstAction:Selector!){
        
        DispatchQueue.main.async() { () -> Void in
            
            let finalString2 = message.inserting(separator: " \n ", every: 35)
            
            let alert:UIAlertController = UIAlertController.init(title: title, message: finalString2, preferredStyle: UIAlertController.Style.alert)
            
            let defaultAction = UIAlertAction(title:firstButtonTitle, style: .default, handler:{(alert:UIAlertAction) -> Void in
                
                if firstAction != nil{
                    controller.perform(firstAction)
                }
            })
            alert.addAction(defaultAction)
            controller.present(alert, animated: true, completion: nil)
        }
    }
    
    /**
     This method is used to add shadow
     parameters :
     targetView : target view on which we want to add shadow
     radius : define the radius on the target view
     */
//    static func addShadow(targetView:AnyObject,radius:CGFloat){
//        targetView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
//        targetView.layer.shadowOffset = CGSize.zero
//        targetView.layer.masksToBounds = false
//        targetView.layer.shadowRadius = radius
//        targetView.layer.shadowOpacity = 0.5
//    }
    
    /**
     This method is used to add shadow only on bottom and right
     parameters :
     targetView : target view on which we want to add shadow
     radius : define the radius on the target view
     */
//    static func addShadowBottomRight(targetView:AnyObject,radius:CGFloat){
//        targetView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7).cgColor
//        targetView.layer.shadowOffset = CGSize(width: 1.0, height: 2.0)
//        targetView.layer.masksToBounds = false
//        targetView.layer.shadowRadius = radius
//        targetView.layer.shadowOpacity = 0.5
//    }
    
    /**
     This method used for shifting views when keyboard appear
     */
    static func animateViewMoving (up:Bool, moveValue :CGFloat, view_controller:UIViewController){
        
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        view_controller.view.frame = view_controller.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }
    
    /*
     this function is used to get network ip
     return : return ip in string value
     */
    static func getIPAddress() -> String? {
        var address : String?
        
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return nil }
        guard let firstAddr = ifaddr else { return nil }
        
        // For each interface ...
        for ifptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let interface = ifptr.pointee
            
            // Check for IPv4 or IPv6 interface:
            let addrFamily = interface.ifa_addr.pointee.sa_family
            if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                
                // Check interface name:
                let name = String(cString: interface.ifa_name)
                if  name == "en0" || name == "pdp_ip0" {
                    
                    // Convert interface address to a human readable string:
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    getnameinfo(interface.ifa_addr, socklen_t(interface.ifa_addr.pointee.sa_len),
                                &hostname, socklen_t(hostname.count),
                                nil, socklen_t(0), NI_NUMERICHOST)
                    address = String(cString: hostname)
                }
            }
        }
        freeifaddrs(ifaddr)
        
        return address
    }
    
    /**
     This method check that give string message in valid email id or not
     - Parameters:
     - testStr: This method take the emial id as string format
     - returns: true or false
     */
    static  func isValidEmail(_ testStr:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr) as Bool
        
        return result
    }
    
    
    /**
     This method check that give string message is in numeric value or not
     - Parameters:
     - value: This method take the value as string format
     - returns: true or false
     */
    
    static func onlyNumeric(value: String) -> Bool {
        let PHONE_REGEX = "^[0-9]*$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    /**
     This function to get time from date string format
     parameters : date in nsdate formate
     return : return date in string
     */
    static func getformatedTimeValue(dateValueString : String) -> String{
        //let newDataVAlue = "2020-01-24 17:17:15"
        var dateStr = dateValueString
        
        if let dotRange = dateStr.range(of: ".") {
            dateStr.removeSubrange(dotRange.lowerBound..<dateStr.endIndex)
        }

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let yourDate = formatter.date(from: dateStr)
        formatter.dateFormat = "hh:mm a"
        let myStringafd = formatter.string(from: yourDate!)
        
        return myStringafd
    }
    
    /**
     This method check is used to fetch current date and time
     - returns: Return current date and time in specific format and in string type
     */
    
    static func getCurrentDateINFormart(formatString : String) -> String {
        let currentDate = Date()
        
        let formatter = DateFormatter()
        formatter.dateFormat = formatString
        
        return formatter.string(from: currentDate)
    }
    
    /**
     This method check is used to fetch current date and time
     - returns: Return current date and time in specific format and in date type
     */
    
    static func changeGetDateFormat(formatString : String) -> String {
        let currentDate = Date()
        
        let formatter = DateFormatter()
        formatter.dateFormat = formatString
        
        let newDate = formatter.string(from: currentDate)
        let convertedDate = formatter.date(from: newDate)!
        
        let changedDate = convertedDate.addingTimeInterval(23.5*(60*60))
        
        let newformatter = DateFormatter()
        newformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let changedDateString = newformatter.string(from: changedDate)
        
        return changedDateString
    }
    
    /**
     This method check is used to compare two time and check time difference
    - Parameters:
        This method takes two time value last calling api time and current time
     - returns: Return true if difference is more than 30min else return false
     */
    
    static func compareTwoDateTime(dateTime1 : String, dateTime2: String, addTime: Bool) -> Bool {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let currentData = formatter.date(from: dateTime1)
        let savedDate = formatter.date(from: dateTime2)
        
        let currentTimeStamp:TimeInterval = currentData!.timeIntervalSince1970
        let savedDateTimeStamp:TimeInterval = savedDate!.timeIntervalSince1970
        
        if addTime == false {
            if (savedDateTimeStamp > currentTimeStamp) {
                return true
            }
            else {
                return false
            }
        }
        else {
            if (savedDateTimeStamp + 1800 >= currentTimeStamp) {
                return false
            }
            else {
                return true
            }
        }
    }
    
    //TODO :- Func to check internet connectivity
    static func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}
