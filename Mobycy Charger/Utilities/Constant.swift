//
//  Constant.swift
//  Mobycy Charger
//
//  Created by sarvesh singh on 1/14/20.
//  Copyright © 2020 Zypp. All rights reserved.
//

import UIKit
import Foundation

//URL Type
let urlType = 1 //0 for staging and 1 for live

//Login
let Check_Mobile_Exist_DB = "check/mobile/"
let User_Login_Api = "secure/user/login"
let User_OTP_Verify_Api = "/verify/forgot/password"
let User_Forgot_Password_Api = "user/generate/otp"
let Set_User_New_Password = "user/forgot/password"

//Authentication
let User_Authentication_Check = "/auth/check"
let User_Authentication_Request = "secure/request/authorization"

//Punching Api
let Get_Punch_Status_Api = "ops/secure/attendence/punchStatus"
let Punch_In_Out_Api = "ops/secure/attendence/tracker"

struct AlertMessage {
    static let Main_Alert_Message = "Mobycy Charger Alert!"
    static let No_Internet = "Look like, You are not connected to internet"
    static let Failed_To_Connect_Server = "Failed to connect api to the server"
    static let Some_Thing_Wrong = "Something went wrong! Please try after some time."
    
    // Add Patient Screen
    static let Enter_Name = "Please enter correct name"
    static let Enter_Email_ID = "Please enter correct email"
    static let Enter_Mobile_No = "Please enter mobile number"
    static let Enter_Valid_Age = "Please enter age in numeric only"
    static let Enter_Valid_Mobile = "Please enter valid mobile number"
    static let Enter_Valid_Email = "Please enter valid email"
    
    static let Enter_Your_Name = "Please enter your name"
    static let Enter_Your_Email = "Please enter your email"
    static let Enter_Your_Password = "Please enter your password!"
    static let Set_Password_Length = "Password should be of minimum 6 characters."
    
    // Apply offer message
    static let Apply_Coupon_Code = "Please enter offer code!"
}


struct toastMessage {
    static let Mobile_Not_Registered = "Mobile number not registered yet"
}

struct customColor {
    
    static let mainHeaderColor = UIColor(red: 9/255.0, green: 217/255.0, blue: 9/255.0, alpha: 1.0)
    static let mainHeaderColorWithAlpha = UIColor(red: 9/255.0, green: 217/255.0, blue: 9/255.0, alpha: 0.4)
    static let statusBarColor = UIColor(red: 9/255.0, green: 217/255.0, blue: 9/255.0, alpha: 1.0)
    
    static let loaderColor = UIColor(red: 9/255.0, green: 217/255.0, blue: 9/255.0, alpha: 1.0)
    
    static let grayViewBGColor = UIColor(red: 245/255.0, green: 245/255.0, blue: 245/255.0, alpha: 1.0)
    static let newGrayBGColor = UIColor(red: 238/255.0, green: 238/255.0, blue: 238/255.0, alpha: 1.0)
    static let grayDrakBGColor = UIColor(red: 225/255.0, green: 225/255.0, blue: 225/255.0, alpha: 1.0)
    static let grayBorderColor = UIColor(red: 200/255.0, green: 200/255.0, blue: 200/255.0, alpha: 1.0)
    static let lightGrayBGColor = UIColor(red: 250/255.0, green: 250/255.0, blue: 250/255.0, alpha: 1.0)
    static let lightGrayTextColor = UIColor(red: 120/255.0, green: 120/255.0, blue: 120/255.0, alpha: 1.0)
    
    // Tab Bar Gray Color
    static let tabBarGrayColor = UIColor(red: 120/255.0, green: 120/255.0, blue: 120/255.0, alpha: 1.0)
    
    // Complete White Color
    static let completeWhiteColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
    
    // Complete Black color
    static let textCompleteBlackColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
    
    //black color with alpha value
    static let blackColorWithOpacity = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.6)
    
    //Black color
    static let lightBlackColor = UIColor(red: 40/255.0, green: 40/255.0, blue: 40/255.0, alpha: 1.0)
    
    // text heading black color
    static let textHeadingBlackColor = UIColor(red: 42/255.0, green: 42/255.0, blue: 42/255.0, alpha: 1.0)
    
    // text sub heading gray color
    static let textSubGrayColor = UIColor(red: 80/255.0, green: 80/255.0, blue: 80/255.0, alpha: 1.0)
    
    // text sub heading gray color
    static let textSubHeadingGrayColor = UIColor(red: 110/255.0, green: 110/255.0, blue: 110/255.0, alpha: 1.0)
    
}


struct FontSize {
    
    static let headingFont10 = 10.0
    static let headingFont11 = 11.0
    static let headingFont12 = 12.0
    static let headingFont13 = 13.0
    static let headingFont14 = 14.0
    static let headingFont15 = 15.0
    static let headingFont16 = 16.0
    static let headingFont17 = 17.0
    static let headingFont18 = 18.0
    static let headingFont19 = 19.0
    static let headingFont20 = 20.0
    static let headingFont22 = 22.0
}

struct TextFontStyle {
    static let fontRegular = "HelveticaNeue-Regular"
    static let fontSemiBold = "HelveticaNeue-Medium"
    static let fontBold = "HelveticaNeue-Bold"
}


//Static key value
struct staticForKey {
    static let device_UDID = "deviceUDID"
}

//Common Response code for authentication
let User_Unauthorize_Code = 9003
let User_Applied_Authorization_Code = 9004
let User_Authentication_Approved = 9006

//Common Response code
let Common_Response_Code = 2000
let Invalid_Mobile_Number = 5027
let User_Not_Register = 4401
let User_Wrong_Credentails = 4404
