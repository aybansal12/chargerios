//
//  Protocols.swift
//  Mobycy Charger
//
//  Created by sarvesh singh on 1/14/20.
//  Copyright © 2020 Zypp. All rights reserved.
//

protocol HomeControllerDelegate {
    func handleMenuToggle(forMenuOption menuOption: MenuOption?)
}

