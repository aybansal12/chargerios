//
//  userDefaultData.swift
//  Mobycy Charger
//
//  Created by sarvesh singh on 1/21/20.
//  Copyright © 2020 Zypp. All rights reserved.
//

import Foundation
extension UserDefaults{
    enum UserDefaultsKeys : String {
        case isLoggedIn
        case userType
        case userID
        case userCartID
        case userName
        case userEmailID
        case userMobileNo
        case userProfileImage
        case deviceToken
        case deviceFcmToken
        case userAccessToken
        case userAuthenticationStatus
        case lastLocationUpdateTime
        case updateLocationStatus
    }
    
    //MARK: Set user login status
    func setUserLogIn(value: String) {
        set(value, forKey: UserDefaultsKeys.isLoggedIn.rawValue)
        //synchronize()
    }
    
    //MARK: Set Device Token
    func setUseAccessToken(value: String) {
        set(value, forKey: UserDefaultsKeys.userAccessToken.rawValue)
        //synchronize()
    }
    
    //MARK: Set User ID
    func setUserID(value: String) {
        set(value, forKey: UserDefaultsKeys.userID.rawValue)
        //synchronize()
    }
    
    //MARK: Set User Name
    func setUserName(value: String) {
        set(value, forKey: UserDefaultsKeys.userName.rawValue)
        //synchronize()
    }
    
    //MARK: Set User Email
    func setUserEmail(value: String) {
        set(value, forKey: UserDefaultsKeys.userEmailID.rawValue)
        //synchronize()
    }
    
    //MARK: Set User Mobile No
    func setUserMobile(value: String) {
        set(value, forKey: UserDefaultsKeys.userMobileNo.rawValue)
        //synchronize()
    }
    
    //MARK: Set User Profile Image
    func setUserProfileImage(value: String) {
        set(value, forKey: UserDefaultsKeys.userProfileImage.rawValue)
        //synchronize()
    }
    
    //MARK: Set User Authentication
    func setUserAuthenticationStatus(value: String) {
        set(value, forKey: UserDefaultsKeys.userAuthenticationStatus.rawValue)
        //synchronize()
    }
    
    //MARK: Set update location time
    func setLastLocationUpdateTime(value: String) {
        set(value, forKey: UserDefaultsKeys.lastLocationUpdateTime.rawValue)
    }
    
    //MARK: Set update location api call or not
    func setUpdateLocationStatus(value: String) {
        set(value, forKey: UserDefaultsKeys.updateLocationStatus.rawValue)
    }
    
     /************************* End Set Data and Start Retriving Data    ******************************/
    
    //TODO: Retrive user login status
    func getUserLogIn() -> String {
        return string(forKey: UserDefaultsKeys.isLoggedIn.rawValue)!
    }
    
    //TODO: Retrive Device Token
    func getUserAccessToken() -> String {
        return string(forKey: UserDefaultsKeys.userAccessToken.rawValue)!
    }
    
    //TODO: Retrive User ID
    func getUserID() -> String {
        return string(forKey: UserDefaultsKeys.userID.rawValue)!
    }
   
    // TODO: Retrive User Name
    func getUserName() -> String {
        return string(forKey: UserDefaultsKeys.userName.rawValue)!
    }
    
    // TODO: Retrive User Email
    func getUserEmail() -> String {
        return string(forKey: UserDefaultsKeys.userEmailID.rawValue)!
    }
    
    // TODO: Retrive User Type
    func getUserMobile() -> String {
        return string(forKey: UserDefaultsKeys.userMobileNo.rawValue)!
    }
    
    // TODO: Retrive User profile image
    func getUserProfileImage() -> String {
        return string(forKey: UserDefaultsKeys.userProfileImage.rawValue)!
    }
    
    // TODO: Retrive User authentication status
    func getUserAuthenticationStatus() -> String {
        return string(forKey: UserDefaultsKeys.userAuthenticationStatus.rawValue)!
    }
    
    // TODO: Retrive User updated time
    func getLastLocationUpdateTime() -> String {
        return string(forKey: UserDefaultsKeys.lastLocationUpdateTime.rawValue)!
    }
    
    // TODO: Retrive update location
    func getUpdateLocationStatus() -> String {
        return string(forKey: UserDefaultsKeys.updateLocationStatus.rawValue)!
    }
}
