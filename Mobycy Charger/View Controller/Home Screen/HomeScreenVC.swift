//
//  HomeScreenVC.swift
//  Mobycy Charger
//
//  Created by sarvesh singh on 1/14/20.
//  Copyright © 2020 Zypp. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation
import GoogleMaps
import UserNotifications

class HomeScreenVC: UIViewController, CLLocationManagerDelegate, UNUserNotificationCenterDelegate {

    @IBOutlet weak var punchBtn: UIButton!
    @IBOutlet weak var logoutBtn: UIButton!
    
    @IBOutlet weak var punchInTimeLbl: UILabel!
    @IBOutlet weak var punchOutTimeLbl: UILabel!
    
    var delegate: HomeControllerDelegate?
    var screenWidth : CGFloat = 0.0
    let defaults = UserDefaults.standard
    var navController = UINavigationController()
    
    var punchDataJSON = JSON()
    
    let blackBtn = UIButton()
    var blackBtnShow = 0
    var punchStatus = 0 //0 for punch in //1 for tracking // 2 for punch out
    let locationManager = CLLocationManager()
    
    var userLatitude : Double = 0.0
    var userLongitude : Double = 0.0
    var locationAccuracy : Double = 0.0
    let content = UNMutableNotificationContent()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let screensize: CGRect = UIScreen.main.bounds
        screenWidth = screensize.width
        
        blackBtn.isHidden = true
        blackBtn.addTarget(self, action: #selector(blackBtnAction), for: .touchUpInside)
        self.view.addSubview(blackBtn)
        //addSlideMenuButton()
        // Do any additional setup after loading the view.
        
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        //self.locationManager.startUpdatingLocation()
        self.locationManager.startMonitoringSignificantLocationChanges()
        self.locationManager.allowsBackgroundLocationUpdates = true
        self.locationManager.pausesLocationUpdatesAutomatically = false
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        }
        
        view.backgroundColor = .white
        
        setPageDesignLayout()
        configureNavigationBar()
        getPunchStatusData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getNotificationLocationUpdates(_:)), name: NSNotification.Name(rawValue: "checkPunchStatusNot"), object: nil)
        
        //UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound], completionHandler: {didAllow, error in})
        
//        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound], completionHandler: { (granted, error) in
//            if granted {
//                // Register after we get the permissions.
//                UNUserNotificationCenter.current().delegate = self
//            }
//        })
    }
    
    //TODO :- Func to get notification is used to fetch punch status
    @objc func getNotificationLocationUpdates(_ notification: NSNotification) {
        getPunchStatusData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @objc func handleMenuToggle() {
        self.handleMenuToggleAction()
    }
    
    //TODO :- Func to handle toogle func
    func handleMenuToggleAction(){
        if blackBtnShow == 0 {
            blackBtnShow = 1
            showHideBlackBtn(btnStatusValue : true)
        }
        else{
            blackBtnShow = 0
            showHideBlackBtn(btnStatusValue : false)
        }
        delegate?.handleMenuToggle(forMenuOption: nil)
    }
    
    func configureNavigationBar() {
        navigationController?.navigationBar.barTintColor = customColor.mainHeaderColor
        navigationController?.navigationBar.barStyle = .black
        
        navigationItem.title = "Attendance"
        //navigationItem.leftBarButtonItem = UIBarButtonItem(image: self.defaultMenuImage().withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleMenuToggle))
    }
    
    func defaultMenuImage() -> UIImage {
        var defaultMenuImage = UIImage()
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 30, height: 22), false, 0.0)
        
        UIColor.black.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 3, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 10, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 17, width: 30, height: 1)).fill()
        
        UIColor.white.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 4, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 11,  width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 18, width: 30, height: 1)).fill()
        
        defaultMenuImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
        
        return defaultMenuImage;
    }
    
    //TODO :- Func to set page design layout
    func setPageDesignLayout(){
        //punchBtn.isHidden = true
        blackBtn.setTitle("", for: .normal)
        blackBtn.backgroundColor = customColor.blackColorWithOpacity
        //blackBtn.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        blackBtn.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        blackBtn.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        blackBtn.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        //blackBtn.heightAnchor.constraint(equalToConstant: self.view.heightAnchor).isActive = true
        blackBtn.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        blackBtn.translatesAutoresizingMaskIntoConstraints = false
        
        punchBtn.layer.cornerRadius = punchBtn.frame.height/2
        punchBtn.layer.masksToBounds = true
        
        punchBtn.setTitleColor(customColor.completeWhiteColor, for: .normal)
        punchBtn.titleLabel?.font = UIFont(name: TextFontStyle.fontSemiBold, size: CGFloat(FontSize.headingFont18))
        punchBtn.backgroundColor = customColor.mainHeaderColor
        
        punchInTimeLbl.textColor = customColor.lightBlackColor
        punchInTimeLbl.font = UIFont(name: TextFontStyle.fontSemiBold, size: CGFloat(FontSize.headingFont14))
        
        punchOutTimeLbl.textColor = customColor.lightBlackColor
        punchOutTimeLbl.font = UIFont(name: TextFontStyle.fontSemiBold, size: CGFloat(FontSize.headingFont14))
        
        //logout btn layout
        logoutBtn.layer.cornerRadius = logoutBtn.frame.height/2
        logoutBtn.layer.masksToBounds = true
        
        logoutBtn.setTitle("Logout", for: .normal)
        logoutBtn.setTitleColor(customColor.completeWhiteColor, for: .normal)
        logoutBtn.titleLabel?.font = UIFont(name: TextFontStyle.fontSemiBold, size: CGFloat(FontSize.headingFont18))
        logoutBtn.backgroundColor = customColor.mainHeaderColor
    }
    
    //TODO :- Func to show and hide black btn
    func showHideBlackBtn(btnStatusValue : Bool){
        if btnStatusValue == true {
            blackBtn.isHidden = false
        }
        else{
            blackBtn.isHidden = true
        }
    }
    
    //TODO :- Perform black btn action
    @objc func blackBtnAction(sender: UIButton!) {
        self.handleMenuToggleAction()
    }
    
    //TODO :- Func to get punch status data
    func getPunchStatusData(){
        let activityIndicator = UIActivityIndicatorView(style:.whiteLarge) // Create the activity indicator
        view.addSubview(activityIndicator) // add it as a  subview
        activityIndicator.center = CGPoint(x: screenWidth*0.5, y: view.frame.size.height*0.5) // put in the middle
        activityIndicator.color = customColor.loaderColor
        activityIndicator.startAnimating() // Start animating
        
        self.view.isUserInteractionEnabled = false
        
        let userAccessToken = "Bearer " + defaults.getUserAccessToken()
        
        let BASE_URL = Utility.returnBaseURL(liveURL: urlType)
        let URL_created = "\(BASE_URL)\(Get_Punch_Status_Api)"
        let headerValue = ["Content-Type":"application/json", "Authorization": userAccessToken]
        
        let paramValues = ["":""] as [String : Any]
        
        if Utility.isConnectedToInternet() {
               ServiceAPI.sharedInstance.callApi(url:URL_created, param_Values:paramValues, header_Value:headerValue, requestMethod:"get", view:self, callbackSuccess: { (Home_Data_JSON) -> Void in
                   
                   activityIndicator.stopAnimating() // On response stop animating
                   activityIndicator.removeFromSuperview() // remove the view
                   
                   self.view.isUserInteractionEnabled = true
                   
                   self.punchDataJSON = JSON(Home_Data_JSON!)
                   
                   let presentStatus = self.punchDataJSON["present"].boolValue
                   if presentStatus == true {
                       if self.punchDataJSON["punchOutTime"].exists() {
                           self.locationManager.stopUpdatingLocation()
                           self.locationManager.stopMonitoringSignificantLocationChanges()
                       }
                       else {
                           self.locationManager.startUpdatingLocation()
                           self.locationManager.startMonitoringSignificantLocationChanges()
                           let locValue = self.locationManager.location?.coordinate
                           if locValue != nil {
                               self.userLatitude = (locValue!.latitude)
                               self.userLongitude = (locValue!.longitude)
                           }
                           self.locationAccuracy = 10.0
                       }
                   }
                   self.setPageData()
                   
               }, callbackFaliure: { (error:String, message:String) -> Void in
                   
                    activityIndicator.stopAnimating() // On response stop animating
                    activityIndicator.removeFromSuperview() // remove the view
                   
                    self.view.isUserInteractionEnabled = true
                    Utility.showAlertView(AlertMessage.Some_Thing_Wrong, title: AlertMessage.Main_Alert_Message)
                    self.failedToConnect()
               })
        }
        else {
            activityIndicator.stopAnimating() // On response stop animating
            activityIndicator.removeFromSuperview() // remove the view
            
            self.view.isUserInteractionEnabled = true
            Utility.showAlertView(AlertMessage.No_Internet, title: AlertMessage.Main_Alert_Message)
            self.failedToConnect()
        }
    }
    
    //TODO :- Func to failed to connect to the internet
    func failedToConnect() {
        self.punchBtn.isHidden = true
        self.punchInTimeLbl.isHidden = true
        self.punchOutTimeLbl.isHidden = true
    }
    
    //TODO :- Func to set page data
    func setPageData(){
        let presentStatus = self.punchDataJSON["present"].boolValue
        let punchInTime = self.punchDataJSON["punchInTime"].stringValue
        let punchOutTime = self.punchDataJSON["punchOutTime"].stringValue
        
        punchInTimeLbl.text = ""
        punchOutTimeLbl.text = ""
        punchBtn.isEnabled = true
        punchBtn.backgroundColor = customColor.mainHeaderColor
        
        if presentStatus == false {
            punchBtn.setTitle("Punch In", for: .normal)
            punchStatus = 0
        }
        else if (presentStatus == true && punchInTime != "" && punchOutTime == ""){
            punchBtn.setTitle("Punch Out", for: .normal)
            punchStatus = 2
            punchInTimeLbl.text = "Punch In: " + Utility.getformatedTimeValue(dateValueString: punchInTime)
        }
        else if (presentStatus == true && punchInTime != "" && punchOutTime != ""){
            punchInTimeLbl.text = "Punch In: " + Utility.getformatedTimeValue(dateValueString: punchInTime)
            punchOutTimeLbl.text = "Punch Out: " + Utility.getformatedTimeValue(dateValueString: punchOutTime)
            
            punchBtn.isEnabled = false
            punchBtn.setTitle("Punch In", for: .normal)
            punchBtn.backgroundColor = customColor.mainHeaderColorWithAlpha
        }
    }
    
    //TODO :- Func to update location status
    func updateLocationDetails() {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                Utility.showAlertControllerAction(message: "In order to make your attendance you need to allow location permission", title: "Location Access Disabled!", onView: self, andFirstButtonTitle: "Cancel", andSecondButtonTitle:"OK", andFirstAction: nil, andSecondAction: #selector(HomeScreenVC.allowLocationPermission))
                
            case .authorizedAlways, .authorizedWhenInUse:
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                self.locationManager.startMonitoringSignificantLocationChanges()
                
                let locValue = self.locationManager.location?.coordinate
                if locValue != nil {
                    self.userLatitude = (locValue!.latitude)
                    self.userLongitude = (locValue!.longitude)
                }
                self.locationAccuracy = 10.0
                
                punchInOutApiCall(btnAction: true)
            }
        } else {
            Utility.showAlertControllerAction(message: "In order to make your attendance you need to allow location permission", title: "Location Access Disabled!", onView: self, andFirstButtonTitle: "Cancel", andSecondButtonTitle:"OK", andFirstAction: nil, andSecondAction: #selector(HomeScreenVC.allowLocationPermission))
        }
    }
    
    //TODO :- Func to set punch btn action
    @IBAction func punchBtnAction(_ sender: Any) {
        updateLocationDetails()
    }
    
    // TODO :- function to allow location permission
    @objc func allowLocationPermission(){
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in })
        }
    }
    
    //TODO :- Func to update user location
    func updateUserLocation() {
        
        var callApi = 0
        
        var updateLocationStatus = ""
        if (defaults.object(forKey:UserDefaults.UserDefaultsKeys.updateLocationStatus.rawValue) != nil) {
            updateLocationStatus = defaults.getUpdateLocationStatus()
        }
        else{
            updateLocationStatus = ""
        }
        
        if updateLocationStatus == "true" {
            
            //callApi = 1
            var updatedTime = ""
            if (defaults.object(forKey:UserDefaults.UserDefaultsKeys.lastLocationUpdateTime.rawValue) != nil) {
                updatedTime = defaults.getLastLocationUpdateTime()
            }
            else{
                updatedTime = ""
            }
            
            if (updatedTime != "") {
                let currentDate = Utility.getCurrentDateINFormart(formatString: "yyyy-MM-dd HH:mm:ss")
                
                let stopServiceTime = Utility.changeGetDateFormat(formatString: "yyyy-MM-dd")
                let callApiStatus = Utility.compareTwoDateTime(dateTime1 : currentDate, dateTime2: stopServiceTime, addTime: false)
                
                if callApiStatus == true {
                    let callApiStatus = Utility.compareTwoDateTime(dateTime1 : currentDate, dateTime2: updatedTime, addTime: true)
                    if callApiStatus == true {
                        callApi = 1
                    }
                    else {
                        callApi = 0
                    }
                }
                else {
                    callApi = 0
                    self.defaults.setLastLocationUpdateTime(value: "")
                    self.defaults.setUpdateLocationStatus(value: "false")
                    self.locationManager.stopUpdatingLocation()
                    self.locationManager.stopMonitoringSignificantLocationChanges()
                }
            }
            else {
                callApi = 0
            }
        }
        else {
            callApi = 0
        }
        
        if callApi == 1 {
            punchInOutApiCall(btnAction: false)
        }
    }
    
    //TODO :- func to call punch in or punch out api
    func punchInOutApiCall(btnAction: Bool){
        
        GMSGeocoder().reverseGeocodeCoordinate(CLLocationCoordinate2DMake(CLLocationDegrees(self.userLatitude), CLLocationDegrees(self.userLongitude)), completionHandler: { response, error in

            if (response?.results()?.count ?? 0) > 0 && response?.results()?.first != nil {
                let addressObj = response?.results()?.first
                var CompleteAddress = ""
                CompleteAddress = (addressObj?.lines?.first)!
                self.newPunchInOutApiCall(btnAction: btnAction, locationString : CompleteAddress)
            }
        })
    }
    
    //TODO :- Func to call punch in out status api
    func newPunchInOutApiCall(btnAction: Bool, locationString : String){
        let mockStatus = false
//        if TARGET_IPHONE_SIMULATOR != 1 {
//
//            let stringToWrite = "Jailbreak Test"
//            do {
//                try stringToWrite.write(toFile:"/private/JailbreakTest.txt", atomically:true, encoding:String.Encoding.utf8)
//                //Device is jailbroken
//        //                return true
//                mockStatus = true
//            }
//            catch {
//                mockStatus = false
//            }
//        }
                
        let activityIndicator = UIActivityIndicatorView(style:.whiteLarge) // Create the activity indicator
        view.addSubview(activityIndicator) // add it as a  subview
        activityIndicator.center = CGPoint(x: screenWidth*0.5, y: view.frame.size.height*0.5) // put in the middle
        activityIndicator.color = customColor.loaderColor
        activityIndicator.startAnimating() // Start animating
        
        self.view.isUserInteractionEnabled = false
        
        let userAccessToken = "Bearer " + defaults.getUserAccessToken()
        
        let BASE_URL = Utility.returnBaseURL(liveURL: urlType)
        let URL_created = "\(BASE_URL)\(Punch_In_Out_Api)"
        let headerValue = ["Content-Type":"application/json", "Authorization": userAccessToken]
        
        let deviceUDID = UIDevice.current.identifierForVendor?.uuidString
        let attendanceID = self.punchDataJSON["attendenceId"].intValue
        
        var newPunchStatus = 0
        if btnAction == true {
            newPunchStatus = punchStatus
        }
        else {
            newPunchStatus = 1
        }
        
        let paramValues = ["attendenceId":attendanceID, "trackType":newPunchStatus, "latitude":self.userLatitude, "longitude":self.userLongitude, "accuracy":self.locationAccuracy, "mock":mockStatus, "imei":deviceUDID!, "deviceSame":true, "address": locationString] as [String: Any]
        
        if Utility.isConnectedToInternet() {
            ServiceAPI.sharedInstance.callJSONEncodnigApi(url:URL_created, param_Values:paramValues, header_Value:headerValue, requestMethod:"post", view:self, callbackSuccess: { (Home_Data_JSON) -> Void in
                
                activityIndicator.stopAnimating() // On response stop animating
                activityIndicator.removeFromSuperview() // remove the view
                
                self.view.isUserInteractionEnabled = true
                
                let JSON_Data = JSON(Home_Data_JSON!)
                if btnAction == true {
                    let statusCode = JSON_Data["code"].intValue
                    let statusMessage = JSON_Data["message"].stringValue
                    if statusCode == User_Not_Register {
                        Utility.showAlertView(statusMessage, title: "Alert")
                    }
                    else {
                        self.punchDataJSON = JSON(Home_Data_JSON!)
                        self.setPageData()
                    }
                }
                let currentDate = Utility.getCurrentDateINFormart(formatString: "yyyy-MM-dd HH:mm:ss")
                self.defaults.setLastLocationUpdateTime(value: currentDate)
                
                if self.punchDataJSON["punchOutTime"].exists() {
                    self.defaults.setLastLocationUpdateTime(value: "")
                    self.defaults.setUpdateLocationStatus(value: "false")
                    self.locationManager.stopUpdatingLocation()
                    self.locationManager.stopMonitoringSignificantLocationChanges()
                }
                else {
                    self.defaults.setUpdateLocationStatus(value: "true")
                    
                    let presentStatus = self.punchDataJSON["present"].boolValue
                    let punchInTime = self.punchDataJSON["punchInTime"].stringValue
                    let punchOutTime = self.punchDataJSON["punchOutTime"].stringValue
                    
                    if (presentStatus == true && punchInTime != "" && punchOutTime == ""){
                        self.locationManager.startUpdatingLocation()
                        self.locationManager.startMonitoringSignificantLocationChanges()
                    }
                }
                
            }, callbackFaliure: { (error:String, message:String) -> Void in
                
                activityIndicator.stopAnimating() // On response stop animating
                activityIndicator.removeFromSuperview() // remove the view
                
                self.view.isUserInteractionEnabled = true
                Utility.showAlertView(AlertMessage.Some_Thing_Wrong, title: AlertMessage.Main_Alert_Message)
            })
        }
        else {
            activityIndicator.stopAnimating() // On response stop animating
            activityIndicator.removeFromSuperview() // remove the view
            
            self.view.isUserInteractionEnabled = true
            Utility.showAlertView(AlertMessage.No_Internet, title: AlertMessage.Main_Alert_Message)
        }
    }
    
    
    //TODO :- Func to set logout btn action
    @IBAction func logoutBtnAction(_ sender: Any) {
        Utility.showAlertControllerAction(message: "Do you want to logout?", title: "Logout", onView: self, andFirstButtonTitle: "No", andSecondButtonTitle:"Yes", andFirstAction: nil, andSecondAction: #selector(HomeScreenVC.logoutUser))
        
//        scheduleNotification()
//        UIApplication.shared.setMinimumBackgroundFetchInterval(60)
    }
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // Create url which from we will get fresh data
       print("data fetch successfully")
    }
    
    // TODO :- logout function
    @objc func logoutUser(){
            
        self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.userID.rawValue)
        self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.userName.rawValue)
        self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.userMobileNo.rawValue)
        self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.isLoggedIn.rawValue)
            
        self.defaults.setUserLogIn(value: "false")
            
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //appDelegate.window?.rootViewController = LoginScreenVC()
            
        self.navController = UINavigationController(rootViewController: LoginScreenVC())
        self.navController.setNavigationBarHidden(true, animated: false)
        appDelegate.window?.rootViewController = self.navController
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("location update \(locations)")
        self.updateUserLocation()
    }
    

    //Alarm function
//    func scheduleNotification(date:String , time: String , subject:String) {
//
//        var dateString = date+" "+time
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
//
//        let convertedDate = dateFormatter.date(from: dateString)!
//
//        let subtractTime = Calendar.current.date(byAdding: .minute, value: -10, to: convertedDate)
//
//        dateString = dateFormatter.string(from: subtractTime!)
//
//        var localTimeZoneName: String { return TimeZone.current.identifier }
//        var secondsFromGMT: Int { return TimeZone.current.secondsFromGMT() }
//        dateFormatter.timeZone = TimeZone(secondsFromGMT: secondsFromGMT)
//        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
//
//        let dateObj:Date = dateFormatter.date(from: dateString)!
//
//        print("alaram time  : \(dateObj)")
//
//        let triggerDaily = Calendar.current.dateComponents([.day,.month,.year,.hour,.minute,], from: dateObj)
//
//
//        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDaily, repeats: true)
//
//        let alarmId = UUID().uuidString
//
//        let content = UNMutableNotificationContent()
//        content.title = "your title"
//        content.body = subject
//        content.sound = UNNotificationSound.init(named: UNNotificationSoundName(rawValue: "your sound filename.mp3"))
//        content.categoryIdentifier = alarmId
//
//        let request = UNNotificationRequest(identifier: "timer", content: content, trigger: trigger)
//
//        UNUserNotificationCenter.current().delegate = self
//        UNUserNotificationCenter.current().add(request) {(error) in
//
//            if let error = error {
//                print("Uh oh! i had an error: \(error)")
//            }
//        }
//    }
    
    func scheduleNotification() {
        let alarmId = UUID().uuidString
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
        content.title = "your title"
        content.body = "You are genius"
        //content.sound = UNNotificationSound.init(named: UNNotificationSoundName(rawValue: "your sound filename.mp3"))
        content.categoryIdentifier = alarmId

        let request = UNNotificationRequest(identifier: "timer", content: content, trigger: trigger)

        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().add(request) {(error) in

            if let error = error {
                print("Uh oh! i had an error: \(error)")
            }
            else{
                print("Ayush Bansal")
            }
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // Update the app interface directly.
        
        // Play a sound.
        print("will present app")
        completionHandler(UNNotificationPresentationOptions.sound)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
         //do sth
        
        if response.actionIdentifier == UNNotificationDismissActionIdentifier {
            print("user dissmiss an app")
        }
        else if response.actionIdentifier == UNNotificationDefaultActionIdentifier {
            print("user lunch an app")
        }
        else {
            print("user ignore an app")
        }
    }
    
//    func playSound(_ soundName: String) {
//
//        //vibrate phone first
//        AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
//        //set vibrate callback
//        AudioServicesAddSystemSoundCompletion(SystemSoundID(kSystemSoundID_Vibrate),nil,
//                                              nil,
//                                              { (_:SystemSoundID, _:UnsafeMutableRawPointer?) -> Void in
//                                                print("callback", terminator: "") //todo
//        },
//                                              nil)
//        let url = URL(
//            fileURLWithPath: Bundle.main.path(forResource: soundName, ofType: "mp3")!)
//
//        var error: NSError?
//
//        do {
//            audioPlayer = try AVAudioPlayer(contentsOf: url)
//        } catch let error1 as NSError {
//            error = error1
//            audioPlayer = nil
//        }
//
//        if let err = error {
//            print("audioPlayer error \(err.localizedDescription)")
//        } else {
//            audioPlayer!.delegate = self
//            audioPlayer!.prepareToPlay()
//        }
//        //negative number means loop infinity
//        audioPlayer!.numberOfLoops = -1
//        audioPlayer!.play()
//    }
    
    
}
