//
//  AuthenticationScreenVC.swift
//  Mobycy Charger
//
//  Created by sarvesh singh on 1/24/20.
//  Copyright © 2020 Zypp. All rights reserved.
//

import UIKit
import SwiftyJSON

class AuthenticationScreenVC: UIViewController, navBakButtonWithPressed {

    @IBOutlet weak var statusBarView: UIView!
    @IBOutlet weak var navHeaderBarView: UIView!
    
    @IBOutlet weak var mesgLbl: UILabel!
    @IBOutlet weak var authSubmitBtn: UIButton!
    @IBOutlet weak var logoutBtn: UIButton!
    
    var navBarView : NavBackView?
    var screenWidth : CGFloat = 0.0
    
    var callAuthApi : Int!
    var authDataJSON = JSON()
    let defaults = UserDefaults.standard
    var navController = UINavigationController()
    
    var userAuthenticationCode = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let screensize: CGRect = UIScreen.main.bounds
        screenWidth = screensize.width
        
        navBarView = Bundle.main.loadNibNamed("NavBackView", owner: self, options: nil)?.first as? NavBackView
        navBarView?.delegate = self
        navBarView?.navTitleLabel.text = "Authentication"
        navBarView?.navBackButton.isHidden = true
        self.navHeaderBarView.addSubview(navBarView!)
        
        if callAuthApi == 1 {
            checkUserAuthStatus()
        }
        else {
            setPageData()
        }
        pageDesignLayout()
    }


    //TODO :- Did Press Back Button
    func didPressButton(button: UIButton) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: false, completion: nil)
    }
    
    //TODO :- Func to set page design layout
    func pageDesignLayout(){
        statusBarView.backgroundColor = customColor.mainHeaderColor
        
        mesgLbl.textColor = customColor.textCompleteBlackColor
        mesgLbl.font = UIFont(name: TextFontStyle.fontSemiBold, size: CGFloat(FontSize.headingFont16))
        
        authSubmitBtn.layer.cornerRadius = authSubmitBtn.frame.height/2
        authSubmitBtn.layer.masksToBounds = true
        
        authSubmitBtn.setTitleColor(customColor.completeWhiteColor, for: .normal)
        authSubmitBtn.titleLabel?.font = UIFont(name: TextFontStyle.fontSemiBold, size: CGFloat(FontSize.headingFont18))
        authSubmitBtn.backgroundColor = customColor.mainHeaderColor
        
        logoutBtn.layer.cornerRadius = logoutBtn.frame.height/2
        logoutBtn.layer.masksToBounds = true
        
        logoutBtn.setTitle("Logout", for: .normal)
        logoutBtn.setTitleColor(customColor.completeWhiteColor, for: .normal)
        logoutBtn.titleLabel?.font = UIFont(name: TextFontStyle.fontSemiBold, size: CGFloat(FontSize.headingFont18))
        logoutBtn.backgroundColor = customColor.mainHeaderColor
    }
    
    //TODO :- Func to set page data
    func setPageData(){
        let statusCode = authDataJSON["code"].intValue
        let mesgString = authDataJSON["message"].stringValue
        
        mesgLbl.text = mesgString
        self.userAuthenticationCode = statusCode
        
        if statusCode == User_Unauthorize_Code {
            authSubmitBtn.isHidden = false
            authSubmitBtn.setTitle("Request Authentication", for: .normal)
        }
        else if statusCode == User_Applied_Authorization_Code {
            authSubmitBtn.isHidden = false
            authSubmitBtn.setTitle("Refresh Status", for: .normal)
        }
        else {
            authSubmitBtn.isHidden = true
        }
    }
    
    //TODO :- Func to check user auth status
    func checkUserAuthStatus(){
        
        let activityIndicator = UIActivityIndicatorView(style:.whiteLarge) // Create the activity indicator
        view.addSubview(activityIndicator) // add it as a  subview
        activityIndicator.center = CGPoint(x: screenWidth*0.5, y: view.frame.size.height*0.5) // put in the middle
        activityIndicator.color = customColor.loaderColor
        activityIndicator.startAnimating() // Start animating
        
        self.view.isUserInteractionEnabled = false
        
        var userMobile = ""
        if (defaults.object(forKey:UserDefaults.UserDefaultsKeys.userMobileNo.rawValue) != nil) {
            userMobile = defaults.getUserMobile()
        }
        else{
            userMobile = ""
        }
       
        var userAccessToken = ""
        if (defaults.object(forKey:UserDefaults.UserDefaultsKeys.userAccessToken.rawValue) != nil) {
            userAccessToken = defaults.getUserAccessToken()
        }
        else{
            userAccessToken = ""
        }
        
        let BASE_URL = Utility.returnBaseURL(liveURL: urlType)
        let URL_created = "\(BASE_URL)" + "ops/\(userMobile)\(User_Authentication_Check)"
        
        let userAccess_Token = "Bearer " + userAccessToken
        let headerValue = ["Content-Type":"application/json", "Authorization": userAccess_Token]
        
        let paramValues = ["":""] as [String : Any]
        
        ServiceAPI.sharedInstance.callApi(url:URL_created, param_Values:paramValues, header_Value:headerValue, requestMethod:"get", view:self, callbackSuccess: { (Home_Data_JSON) -> Void in
            
            activityIndicator.stopAnimating() // On response stop animating
            activityIndicator.removeFromSuperview() // remove the view
            
            self.view.isUserInteractionEnabled = true
            
            let JSON_Data = JSON(Home_Data_JSON!)
            self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.userAuthenticationStatus.rawValue)
            let statusCode = JSON_Data["code"].intValue
            let mesgString = JSON_Data["message"].stringValue
            
            if (statusCode == User_Authentication_Approved || statusCode == 0) {
                
                self.defaults.setUserAuthenticationStatus(value: "true")
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = ContainerController()
                
                let nextVC = HomeScreenVC()
                self.navigationController?.pushViewController(nextVC, animated: true)
            }
            else{
                self.view.showToast(toastMessage: mesgString, duration: 1.5)
                self.defaults.setUserAuthenticationStatus(value: "false")
                
                self.authDataJSON = JSON_Data
                self.setPageData()
            }
            
        }, callbackFaliure: { (error:String, message:String) -> Void in
            
            activityIndicator.stopAnimating() // On response stop animating
            activityIndicator.removeFromSuperview() // remove the view
            
            self.view.isUserInteractionEnabled = true
            self.view.showToast(toastMessage: message, duration: 1.5)
        })
    }
    
    //TODO :- Func to set auth submit btn action
    @IBAction func authBtnSubmitAction(_ sender: Any) {
        
        if userAuthenticationCode == User_Unauthorize_Code {
            let activityIndicator = UIActivityIndicatorView(style:.whiteLarge) // Create the activity indicator
            view.addSubview(activityIndicator) // add it as a  subview
            activityIndicator.center = CGPoint(x: screenWidth*0.5, y: view.frame.size.height*0.5) // put in the middle
            activityIndicator.color = customColor.loaderColor
            activityIndicator.startAnimating() // Start animating
            
            self.view.isUserInteractionEnabled = false
            
            var userAccessToken = ""
            if (defaults.object(forKey:UserDefaults.UserDefaultsKeys.userAccessToken.rawValue) != nil) {
                userAccessToken = defaults.getUserAccessToken()
            }
            else{
                userAccessToken = ""
            }
            
            let BASE_URL = Utility.returnBaseURL(liveURL: urlType)
            let URL_created = "\(BASE_URL)" + "\(User_Authentication_Request)"
            
            let userAccess_Token = "Bearer " + userAccessToken
            let headerValue = ["Content-Type":"application/json", "Authorization": userAccess_Token]
            
            let paramValues = ["":""] as [String : Any]
            
            ServiceAPI.sharedInstance.callApi(url:URL_created, param_Values:paramValues, header_Value:headerValue, requestMethod:"get", view:self, callbackSuccess: { (Home_Data_JSON) -> Void in
                
                activityIndicator.stopAnimating() // On response stop animating
                activityIndicator.removeFromSuperview() // remove the view
                
                self.view.isUserInteractionEnabled = true
                
                let JSON_Data = JSON(Home_Data_JSON!)
                self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.userAuthenticationStatus.rawValue)
                let statusCode = JSON_Data["code"].intValue
                let mesgString = JSON_Data["message"].stringValue
                
                if (statusCode == User_Authentication_Approved || statusCode == 0) {
                    self.defaults.setUserAuthenticationStatus(value: "true")
                    
                    self.view.showToast(toastMessage: mesgString, duration: 1.5)
                    
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = ContainerController()
                    
                    let nextVC = HomeScreenVC()
                    self.navigationController?.pushViewController(nextVC, animated: true)
                }
                else {
                    
                    self.defaults.setUserAuthenticationStatus(value: "false")
                    
                    self.authDataJSON = JSON_Data
                    self.setPageData()
                }
                
            }, callbackFaliure: { (error:String, message:String) -> Void in
                
                activityIndicator.stopAnimating() // On response stop animating
                activityIndicator.removeFromSuperview() // remove the view
                
                self.view.isUserInteractionEnabled = true
                self.view.showToast(toastMessage: message, duration: 1.5)
            })
        }
        else if userAuthenticationCode == User_Applied_Authorization_Code {
            self.checkUserAuthStatus()
        }
        else{
            let mesgString = authDataJSON["message"].stringValue
            self.view.showToast(toastMessage: mesgString, duration: 1.5)
        }
    }
    
    //TODO :- Func to set logout btn action 
    @IBAction func logoutBtnAction(_ sender: Any) {
        Utility.showAlertControllerAction(message: "Are you sure you want to logout?", title: "Logout!", onView: self, andFirstButtonTitle: "NOT NOW", andSecondButtonTitle:"YES", andFirstAction: nil, andSecondAction: #selector(AuthenticationScreenVC.logoutUser))
    }
    
    // TODO :- logout function
    @objc func logoutUser(){
            
        self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.userID.rawValue)
        self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.userName.rawValue)
        self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.userMobileNo.rawValue)
        self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.isLoggedIn.rawValue)
            
        self.defaults.setUserLogIn(value: "false")
            
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //appDelegate.window?.rootViewController = LoginScreenVC()
            
        self.navController = UINavigationController(rootViewController: LoginScreenVC())
        self.navController.setNavigationBarHidden(true, animated: false)
        appDelegate.window?.rootViewController = self.navController
            
    //        let nextVC = LoginScreenVC()
    //        self.navigationController?.pushViewController(nextVC, animated: true)
        }
    
}
