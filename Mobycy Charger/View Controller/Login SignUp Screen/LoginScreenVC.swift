//
//  LoginScreenVC.swift
//  Mobycy Charger
//
//  Created by sarvesh singh on 1/14/20.
//  Copyright © 2020 Zypp. All rights reserved.
//

import UIKit
import SwiftyJSON

class LoginScreenVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var dataContainView: UIView!
    @IBOutlet weak var skipBtn: UIButton!
    
    @IBOutlet weak var mobileNoTextField: UITextField!
    @IBOutlet weak var underLineView: UIView!
    
    @IBOutlet weak var loginSignUpBtn: UIButton!
    
    var screenWidth : CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let screensize: CGRect = UIScreen.main.bounds
        screenWidth = screensize.width

        mobileNoTextField.delegate = self
        mobileNoTextField.keyboardType = UIKeyboardType.numberPad
        
        skipBtn.isHidden = true
        pageDesignLayout()
    }
    
    override func touchesBegan(_: Set<UITouch>, with: UIEvent?) {
        mobileNoTextField.resignFirstResponder()
        self.view.endEditing(true)
    }
    
    //TODO :- Func to set page design layout
    func pageDesignLayout(){
        underLineView.isHidden = true
        underLineView.backgroundColor = customColor.grayDrakBGColor
        
        mobileNoTextField.textColor = customColor.lightBlackColor
        mobileNoTextField.layer.cornerRadius = 5.0
        mobileNoTextField.layer.masksToBounds = true;
        mobileNoTextField.layer.borderWidth = 1.0
        mobileNoTextField.layer.borderColor = customColor.grayBorderColor.cgColor
        
        let mobileIconView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        mobileIconView.image = UIImage(named: "Mobile_Icon")
        mobileIconView.contentMode = .center
        mobileNoTextField.leftViewMode = .always
        mobileNoTextField.leftView = mobileIconView
        
        skipBtn.setTitleColor(customColor.textCompleteBlackColor, for: .normal)
        skipBtn.titleLabel?.font = UIFont(name: TextFontStyle.fontSemiBold, size: CGFloat(FontSize.headingFont14))
        
        loginSignUpBtn.layer.cornerRadius = loginSignUpBtn.frame.height/2
        loginSignUpBtn.layer.masksToBounds = true
        
        loginSignUpBtn.setTitle("Login", for: .normal)
        loginSignUpBtn.setTitleColor(customColor.completeWhiteColor, for: .normal)
        loginSignUpBtn.backgroundColor = customColor.mainHeaderColor
        loginSignUpBtn.titleLabel?.font = UIFont(name: TextFontStyle.fontSemiBold, size: CGFloat(FontSize.headingFont17))
    }

    //TODO:- Text Field Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        self.view.endEditing(true)
        return true
    }
    
    // TODO :- Function to check mobile character count
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
        if textField == mobileNoTextField {
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            if newLength == 11 {
                self.view.endEditing(true)
            }
            return  newLength <= 10
        }
        else{
            return true
        }
    }

    // TODO :- Skip Btn Action
    @IBAction func skipBtnAction(_ sender: Any) {
        print("skip btn action")
    }
    
    //TODO :- Login Signup Btn action
    @IBAction func loginSignUpBtnAction(_ sender: Any) {
       
        if mobileNoTextField.text == ""{
            Utility.showAlertView(AlertMessage.Enter_Mobile_No, title: AlertMessage.Main_Alert_Message)
        }
        else{
            let validMobile = Utility.onlyNumeric(value: mobileNoTextField.text!)
            if validMobile == false {
                Utility.showAlertView(AlertMessage.Enter_Valid_Mobile, title: AlertMessage.Main_Alert_Message)
            }
            else{
                callLoginApi()
            }
        }
        //callLoginApi()
    }
    
    // TODO :- Function to call login
    func callLoginApi(){
        
        let activityIndicator = UIActivityIndicatorView(style:.whiteLarge) // Create the activity indicator
        view.addSubview(activityIndicator) // add it as a  subview
        activityIndicator.center = CGPoint(x: screenWidth*0.5, y: view.frame.size.height*0.5) // put in the middle
        activityIndicator.color = customColor.loaderColor
        activityIndicator.startAnimating() // Start animating
        
        self.view.isUserInteractionEnabled = false
        
        let BASE_URL = Utility.returnBaseURL(liveURL: urlType)
        
        let URL_created = "\(BASE_URL)\(Check_Mobile_Exist_DB)\(mobileNoTextField.text!)"
        let headerValue = ["Content-Type":"application/json"]
        
        let paramValues = [:] as [String : Any]
        
        ServiceAPI.sharedInstance.callApi(url:URL_created, param_Values:paramValues, header_Value:headerValue, requestMethod:"get", view:self, callbackSuccess: { (Home_Data_JSON) -> Void in
            
            activityIndicator.stopAnimating() // On response stop animating
            activityIndicator.removeFromSuperview() // remove the view
            
            self.view.isUserInteractionEnabled = true
            
            let JSON_Data = JSON(Home_Data_JSON!)
            let status = JSON_Data["status"].boolValue
            if status == true {
                let nextVC = PasswordOtpVC()
                nextVC.previousScreen = "Login"
                nextVC.mobileNumber = self.mobileNoTextField.text!
                self.navigationController?.pushViewController(nextVC, animated: true)
            }
            else{
                let nextVC = PasswordOtpVC()
                nextVC.previousScreen = "OTP"
                nextVC.mobileNumber = self.mobileNoTextField.text!
                self.navigationController?.pushViewController(nextVC, animated: true)
            }
            self.view.endEditing(true)
            
        }, callbackFaliure: { (error:String, message:String) -> Void in
            
            activityIndicator.stopAnimating() // On response stop animating
            activityIndicator.removeFromSuperview() // remove the view
            
            self.view.isUserInteractionEnabled = true
        })
        
    }
    
}
