//
//  PasswordOtpVC.swift
//  Mobycy Charger
//
//  Created by sarvesh singh on 1/16/20.
//  Copyright © 2020 Zypp. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVPinView

class PasswordOtpVC: UIViewController, navBakButtonWithPressed, UITextFieldDelegate {

    @IBOutlet weak var statusBarView: UIView!
    @IBOutlet weak var navHeaderBarView: UIView!
    
    @IBOutlet weak var loginPasswordView: UIView!
    @IBOutlet weak var loginPasswordTextField: UITextField!
    @IBOutlet weak var loginPasswordLine: UIView!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var forgotPasswordBtn: UIButton!
    
    @IBOutlet weak var otpLoginView: UIView!
    @IBOutlet weak var otpHeadingLbl: UILabel!
    @IBOutlet weak var otpTimeRemLbl: UILabel!
    
    @IBOutlet weak var setPasswordView: UIView!
    @IBOutlet weak var setPasswordTextField: UITextField!
    @IBOutlet weak var setPasswordBtn: UIButton!
    
    @IBOutlet weak var submitOtpBtn: UIButton!
    @IBOutlet var otpPinView:SVPinView!
    
    var navBarView : NavBackView?
    var screenWidth : CGFloat = 0.0
    var screenHeight : CGFloat = 0.0
    let defaults = UserDefaults.standard
    
    var previousScreen : String!
    var mobileNumber : String!
    var userAccess_Token : String!
    
    var btnShowPassword = UIButton()
    var btnSetNewPassword = UIButton()
    var remainingTime = 0
    var timerRemainingInterval : Timer? = nil
    
    var showLoginPage = 0  //0 for login page //1 for forgot password
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let screensize: CGRect = UIScreen.main.bounds
        screenWidth = screensize.width
        screenHeight = screensize.height

        navBarView = Bundle.main.loadNibNamed("NavBackView", owner: self, options: nil)?.first as? NavBackView
        navBarView?.delegate = self
        self.navHeaderBarView.addSubview(navBarView!)
        
        loginPasswordTextField.delegate = self
        setPasswordTextField.delegate = self
        
        remainingTime = 60
        
        setPinViewLayout()
        setPageView()
        setPageDesignLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
    override func touchesBegan(_: Set<UITouch>, with: UIEvent?) {
        loginPasswordTextField.resignFirstResponder()
        setPasswordTextField.resignFirstResponder()
        self.view.endEditing(true)
    }
    
    //TODO :- Did Press Back Button
    func didPressButton(button: UIButton) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: false, completion: nil)
    }
    
    //TODO:- Text Field Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        self.view.endEditing(true)
        return true
    }
    
    //MARK: - TextFiled Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if screenHeight < 570 {
            Utility.animateViewMoving(up: true, moveValue: 75,view_controller: self)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if screenHeight < 570 {
            Utility.animateViewMoving(up: false, moveValue: 75,view_controller: self)
        }
    }
    
    //TODO :- Func to perform set page view
    func setPageView(){
        setPasswordView.isHidden = true
        if previousScreen == "Login" {
            showLoginPage = 0
            navBarView?.navTitleLabel.text = "Password"
            loginPasswordView.isHidden = false
            otpLoginView.isHidden = true

        }
        else{
            showLoginPage = 1
            otpHeadingLbl.text = "Enjoy the breeze, while we verifying your mobile \n number +91" + mobileNumber!
            navBarView?.navTitleLabel.text = "OTP"
            loginPasswordView.isHidden = true
            otpLoginView.isHidden = false
            otpPinView.becomeFirstResponderAtIndex = 0
        }
        
        let passwordImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        passwordImageView.image = UIImage(named: "password")
        passwordImageView.contentMode = .center
        loginPasswordTextField.leftViewMode = .always
        loginPasswordTextField.leftView = passwordImageView
        
        let newPasswordImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        newPasswordImageView.image = UIImage(named: "password")
        newPasswordImageView.contentMode = .center
        setPasswordTextField.leftViewMode = .always
        setPasswordTextField.leftView = newPasswordImageView
        
        btnShowPassword.setImage(UIImage(named: "hidePassword"), for: .normal)
        btnShowPassword.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        btnShowPassword.addTarget(self, action: #selector(showPasswordAction), for: .touchUpInside)
        loginPasswordTextField.rightViewMode = .always
        loginPasswordTextField.rightView = btnShowPassword
        
        btnSetNewPassword.setImage(UIImage(named: "hidePassword"), for: .normal)
        btnSetNewPassword.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        btnSetNewPassword.addTarget(self, action: #selector(setNewPasswordAction), for: .touchUpInside)
        setPasswordTextField.rightViewMode = .always
        setPasswordTextField.rightView = btnSetNewPassword
    }
    
    //TODO :- Func to set page design layout
    func setPageDesignLayout(){
        statusBarView.backgroundColor = customColor.mainHeaderColor
        
        loginPasswordTextField.layer.cornerRadius = 5.0
        loginPasswordTextField.layer.masksToBounds = true;
        loginPasswordTextField.layer.borderWidth = 1.0
        loginPasswordTextField.layer.borderColor = customColor.grayBorderColor.cgColor
        
        loginPasswordTextField.textColor = customColor.lightBlackColor
        loginPasswordTextField.font = UIFont(name: TextFontStyle.fontSemiBold, size: CGFloat(FontSize.headingFont16))
        
        loginPasswordLine.isHidden = true
        loginPasswordLine.backgroundColor = customColor.grayDrakBGColor
        
        loginBtn.layer.cornerRadius = loginBtn.frame.height/2
        loginBtn.layer.masksToBounds = true
        
        loginBtn.setTitle("Login", for: .normal)
        loginBtn.setTitleColor(customColor.completeWhiteColor, for: .normal)
        loginBtn.titleLabel?.font = UIFont(name: TextFontStyle.fontSemiBold, size: CGFloat(FontSize.headingFont18))
        loginBtn.backgroundColor = customColor.mainHeaderColor
        
        
        //otp screen
        otpHeadingLbl.textColor = customColor.textCompleteBlackColor
        otpHeadingLbl.font = UIFont(name: TextFontStyle.fontSemiBold, size: CGFloat(FontSize.headingFont17))
        
        otpTimeRemLbl.textColor = customColor.lightBlackColor
        otpTimeRemLbl.font = UIFont(name: TextFontStyle.fontRegular, size: CGFloat(FontSize.headingFont13))
        
        submitOtpBtn.layer.cornerRadius = submitOtpBtn.frame.height/2
        submitOtpBtn.layer.masksToBounds = true
        
        submitOtpBtn.setTitle("Submit", for: .normal)
        submitOtpBtn.setTitleColor(customColor.completeWhiteColor, for: .normal)
        submitOtpBtn.titleLabel?.font = UIFont(name: TextFontStyle.fontSemiBold, size: CGFloat(FontSize.headingFont18))
        submitOtpBtn.backgroundColor = customColor.mainHeaderColor
        
        forgotPasswordBtn.setTitle("Forgot Password?", for: .normal)
        forgotPasswordBtn.setTitleColor(customColor.textCompleteBlackColor, for: .normal)
        forgotPasswordBtn.titleLabel?.font = UIFont(name: TextFontStyle.fontSemiBold, size: CGFloat(FontSize.headingFont14))
        
        //set new password screen
        setPasswordTextField.layer.cornerRadius = 5.0
        setPasswordTextField.layer.masksToBounds = true;
        setPasswordTextField.layer.borderWidth = 1.0
        setPasswordTextField.layer.borderColor = customColor.grayBorderColor.cgColor
        
        setPasswordTextField.textColor = customColor.lightBlackColor
        setPasswordTextField.font = UIFont(name: TextFontStyle.fontSemiBold, size: CGFloat(FontSize.headingFont16))
    
        setPasswordBtn.layer.cornerRadius = loginBtn.frame.height/2
        setPasswordBtn.layer.masksToBounds = true
        
        setPasswordBtn.setTitle("Save", for: .normal)
        setPasswordBtn.setTitleColor(customColor.completeWhiteColor, for: .normal)
        setPasswordBtn.titleLabel?.font = UIFont(name: TextFontStyle.fontSemiBold, size: CGFloat(FontSize.headingFont18))
        setPasswordBtn.backgroundColor = customColor.mainHeaderColor
    }
    
    //TODO :- Func to set otp data view
    func setPinViewLayout(){
        let style = SVPinViewStyle(rawValue: 1)!
        
        otpPinView.activeBorderLineThickness = 2
        otpPinView.fieldBackgroundColor = UIColor.clear
        otpPinView.activeFieldBackgroundColor = UIColor.clear
        otpPinView.fieldCornerRadius = 0
        otpPinView.activeFieldCornerRadius = 0
        otpPinView.style = style
        
        otpPinView.pinLength = 6
        //pinView.secureCharacter = "\u{25CF}"
        //otpPinView.interSpace = 10
        otpPinView.textColor = customColor.mainHeaderColor
        otpPinView.borderLineColor = customColor.grayDrakBGColor
        otpPinView.activeBorderLineColor = customColor.mainHeaderColor
        otpPinView.shouldSecureText = false
        otpPinView.allowsWhitespaces = false
        otpPinView.fieldBackgroundColor = UIColor.white.withAlphaComponent(0.3)
        otpPinView.activeFieldBackgroundColor = UIColor.white.withAlphaComponent(0.5)
        //otpPinView.becomeFirstResponderAtIndex = 0
        
        otpPinView.font = UIFont(name: TextFontStyle.fontSemiBold, size: CGFloat(FontSize.headingFont18))!
    }
    
    func didFinishEnteringPin(pin:String) {
        callOTPVerifyApi()
    }
    
    //TODO :- Func to perform action to show passoword
    @objc func showPasswordAction(){
        if (self.loginPasswordTextField.isSecureTextEntry){
            self.loginPasswordTextField.isSecureTextEntry = false;
            btnShowPassword.setImage(UIImage(named: "show_Password"), for: .normal)
        }
        else{
            self.loginPasswordTextField.isSecureTextEntry = true;
            btnShowPassword.setImage(UIImage(named: "hidePassword"), for: .normal)
        }
    }
    
    @objc func setNewPasswordAction(){
        if (self.setPasswordTextField.isSecureTextEntry){
            self.setPasswordTextField.isSecureTextEntry = false;
            btnSetNewPassword.setImage(UIImage(named: "show_Password"), for: .normal)
        }
        else{
            self.setPasswordTextField.isSecureTextEntry = true;
            btnSetNewPassword.setImage(UIImage(named: "hidePassword"), for: .normal)
        }
    }
    
    //TODO :- Func to perform login btn action
    @IBAction func loginBtnAction(_ sender: Any) {
        
        guard loginPasswordTextField.text != "" else {
            Utility.showAlertView(AlertMessage.Enter_Your_Password, title: AlertMessage.Main_Alert_Message)
            return
        }
        guard (loginPasswordTextField.text?.count)! > 5 else {
            Utility.showAlertView(AlertMessage.Set_Password_Length, title: AlertMessage.Main_Alert_Message)
            return
        }
    
        callDatatApi()
    }
    
    //TODO :- Func to call api
    func callDatatApi(){
        let activityIndicator = UIActivityIndicatorView(style:.whiteLarge) // Create the activity indicator
        view.addSubview(activityIndicator) // add it as a  subview
        activityIndicator.center = CGPoint(x: screenWidth*0.5, y: view.frame.size.height*0.5) // put in the middle
        activityIndicator.color = customColor.loaderColor
        activityIndicator.startAnimating() // Start animating
        
        self.view.isUserInteractionEnabled = false
        
        let BASE_URL = Utility.returnBaseURL(liveURL: urlType)
        let URL_created = "\(BASE_URL)\(User_Login_Api)"
        
        let authStr = "\(mobileNumber!)" + ":" + "\(loginPasswordTextField.text!)"
        let authData = authStr.data(using: .ascii)
        let authValue = "Basic \(authData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 80)) ?? "")"
        
        let headerValue = ["Authorization":authValue]
        
        let paramValues = ["":""] as [String : Any]
        
        if Utility.isConnectedToInternet() {
            ServiceAPI.sharedInstance.callApi(url:URL_created, param_Values:paramValues, header_Value:headerValue, requestMethod:"get", view:self, callbackSuccess: { (Home_Data_JSON) -> Void in
                        
            activityIndicator.stopAnimating() // On response stop animating
            activityIndicator.removeFromSuperview() // remove the view
                        
            self.view.isUserInteractionEnabled = true
                    
            let JSON_Data = JSON(Home_Data_JSON!)
            let statusCode = JSON_Data["code"].intValue
            let statusMessage = JSON_Data["message"].stringValue
            if statusCode == User_Wrong_Credentails {
                Utility.showAlertControllerOneButtonAction(message: statusMessage, title: "Alert", onView: self, andFirstButtonTitle: "OK", andFirstAction: #selector(PasswordOtpVC.wrongCredentialAction))
            }
            else {
                let userID = JSON_Data["userId"].stringValue
                let userName = JSON_Data["name"].stringValue
                let userMobile = JSON_Data["mobile"].stringValue
                let userAccessToken = JSON_Data["accessToken"].stringValue
                let userProfileImage = JSON_Data["profilePicId"].stringValue
                    
                self.defaults.setUserID(value: userID)
                self.defaults.setUserName(value: userName)
                self.defaults.setUserMobile(value: userMobile)
                self.defaults.setUseAccessToken(value: userAccessToken)
                self.defaults.setUserProfileImage(value: userProfileImage)
                self.defaults.setUserLogIn(value: "true")
                    
                self.userAccess_Token = userAccessToken
                self.checkUserAuth()
            }
                        
            //            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            //            appDelegate.window?.rootViewController = ContainerController()
            //
            //            let nextVC = HomeScreenVC()
            //            self.navigationController?.pushViewController(nextVC, animated: true)
                        
        }, callbackFaliure: { (error:String, message:String) -> Void in
                        
                activityIndicator.stopAnimating() // On response stop animating
                activityIndicator.removeFromSuperview() // remove the view
                        
                self.view.isUserInteractionEnabled = true
                self.view.showToast(toastMessage: message, duration: 1.5)
            })
        }
        else {
            activityIndicator.stopAnimating() // On response stop animating
            activityIndicator.removeFromSuperview() // remove the view
            
            self.view.isUserInteractionEnabled = true
            Utility.showAlertView(AlertMessage.No_Internet, title: AlertMessage.Main_Alert_Message)
        }
    }
    
    //TODO :- Func to check user credentials
    @objc func wrongCredentialAction(){
        navigationController?.popViewController(animated: true)
        dismiss(animated: false, completion: nil)
    }
    
    //TODO :- Func to check user authentication
    func checkUserAuth(){
        
        let activityIndicator = UIActivityIndicatorView(style:.whiteLarge) // Create the activity indicator
        view.addSubview(activityIndicator) // add it as a  subview
        activityIndicator.center = CGPoint(x: screenWidth*0.5, y: view.frame.size.height*0.5) // put in the middle
        activityIndicator.color = customColor.loaderColor
        activityIndicator.startAnimating() // Start animating
        
        self.view.isUserInteractionEnabled = false
        
        let BASE_URL = Utility.returnBaseURL(liveURL: urlType)
        let URL_created = "\(BASE_URL)" + "ops/\(mobileNumber!)\(User_Authentication_Check)"
        
        let userAccessToken = "Bearer " + self.userAccess_Token
        let headerValue = ["Content-Type":"application/json", "Authorization": userAccessToken]
        
        let paramValues = ["":""] as [String : Any]
        
        ServiceAPI.sharedInstance.callApi(url:URL_created, param_Values:paramValues, header_Value:headerValue, requestMethod:"get", view:self, callbackSuccess: { (Home_Data_JSON) -> Void in
            
            activityIndicator.stopAnimating() // On response stop animating
            activityIndicator.removeFromSuperview() // remove the view
            
            self.view.isUserInteractionEnabled = true
            
            let JSON_Data = JSON(Home_Data_JSON!)
            
            self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.userAuthenticationStatus.rawValue)
            let statusCode = JSON_Data["code"].intValue
            
            if (statusCode == User_Authentication_Approved || statusCode == 0) {
                self.defaults.setUserAuthenticationStatus(value: "true")
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = ContainerController()
                
                let nextVC = HomeScreenVC()
                self.navigationController?.pushViewController(nextVC, animated: true)
            }
            else{
                self.defaults.setUserAuthenticationStatus(value: "false")
                
                let nextVC = AuthenticationScreenVC()
                nextVC.callAuthApi = 0
                nextVC.authDataJSON = JSON_Data
                self.navigationController?.pushViewController(nextVC, animated: true)
               
        
//                if authString == "ROLE_OPSAPP_CIH" {
//                    self.defaults.setUserAuthenticationStatus(value: "true")
//
//                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                    appDelegate.window?.rootViewController = ContainerController()
//
//                    let nextVC = HomeScreenVC()
//                    self.navigationController?.pushViewController(nextVC, animated: true)
//                }
//                else{
//                    self.defaults.setUserAuthenticationStatus(value: "false")
//
//                    let nextVC = AuthenticationScreenVC()
//                    nextVC.callAuthApi = 0
//                    nextVC.authDataJSON = JSON_Data
//                    self.navigationController?.pushViewController(nextVC, animated: true)
//                }
            }
            
        }, callbackFaliure: { (error:String, message:String) -> Void in
            
            activityIndicator.stopAnimating() // On response stop animating
            activityIndicator.removeFromSuperview() // remove the view
            
            self.view.isUserInteractionEnabled = true
            self.view.showToast(toastMessage: message, duration: 1.5)
        })
    }
    
    //TODO :- Forgot password btn action
    @IBAction func forgotPasswordBtnAction(_ sender: Any) {
        let activityIndicator = UIActivityIndicatorView(style:.whiteLarge) // Create the activity indicator
        view.addSubview(activityIndicator) // add it as a  subview
        activityIndicator.center = CGPoint(x: screenWidth*0.5, y: view.frame.size.height*0.5) // put in the middle
        activityIndicator.color = customColor.loaderColor
        activityIndicator.startAnimating() // Start animating
        
        self.view.isUserInteractionEnabled = false
        
        let BASE_URL = Utility.returnBaseURL(liveURL: urlType)
        let URL_created = "\(BASE_URL)\(User_Forgot_Password_Api)?mobile=\(mobileNumber!)&forgotPwdFlag=\(1)"
        let headerValue = ["Content-Type":"application/json"]
        
        let paramValues = ["":""] as [String : Any]
        
        ServiceAPI.sharedInstance.callApi(url:URL_created, param_Values:paramValues, header_Value:headerValue, requestMethod:"get", view:self, callbackSuccess: { (Home_Data_JSON) -> Void in
            
            activityIndicator.stopAnimating() // On response stop animating
            activityIndicator.removeFromSuperview() // remove the view
            
            self.view.isUserInteractionEnabled = true
            
            let JSON_Data = JSON(Home_Data_JSON!)
            let statusCode = JSON_Data["code"].intValue
            let messageString = JSON_Data["message"].stringValue
            
            if statusCode == Common_Response_Code {
                self.timerRemainingInterval = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.makeTimeValuable(_:)), userInfo: nil, repeats: true)
                self.showLoginPage = 1
                self.showPageFunc()
            }
            else if statusCode == Invalid_Mobile_Number {
                self.view.showToast(toastMessage: toastMessage.Mobile_Not_Registered, duration: 1.5)
            }
            else if messageString != "" {
                self.view.showToast(toastMessage: messageString, duration: 1.5)
            }
            else{
                self.view.showToast(toastMessage: AlertMessage.Some_Thing_Wrong, duration: 1.5)
            }
            
        }, callbackFaliure: { (error:String, message:String) -> Void in
            
            activityIndicator.stopAnimating() // On response stop animating
            activityIndicator.removeFromSuperview() // remove the view
            
            self.view.isUserInteractionEnabled = true
            self.view.showToast(toastMessage: message, duration: 1.5)
        })
    }
    
    //TODO :- Check timer value
    @objc func makeTimeValuable(_ timer: Timer?) {
        
        remainingTime = remainingTime - 1
        if remainingTime >= 0 {
            otpTimeRemLbl.text = "Waiting for OTP (00:\(remainingTime)s)"
        } else {
            timerRemainingInterval!.invalidate()
//            btnResendOTP.isUserInteractionEnabled = true
        }
    }
    
    //TODO :- Func to show and hide view
    func showPageFunc(){
        if self.showLoginPage == 1 {
            self.loginPasswordView.isHidden = true
            self.otpLoginView.isHidden = false
            
            otpHeadingLbl.text = "Enjoy the breeze, while we verifying your mobile \n number +91" + mobileNumber!
            navBarView?.navTitleLabel.text = "OTP"
        }
    }
    
    
    //TODO :- Submit otp btn action
    @IBAction func submitOtpBtnAction(_ sender: Any) {
        
        let pin = otpPinView.getPin()
        guard !pin.isEmpty else {
            Utility.showAlertView("Please enter valid otp", title: "OTP!")
            return
        }
        callOTPVerifyApi()
    }
    
    //TODO :- Func to call otp verify api
    func callOTPVerifyApi(){
        let activityIndicator = UIActivityIndicatorView(style:.whiteLarge) // Create the activity indicator
        view.addSubview(activityIndicator) // add it as a  subview
        activityIndicator.center = CGPoint(x: screenWidth*0.5, y: view.frame.size.height*0.5) // put in the middle
        activityIndicator.color = customColor.loaderColor
        activityIndicator.startAnimating() // Start animating
        
        self.view.isUserInteractionEnabled = false
        
        let BASE_URL = Utility.returnBaseURL(liveURL: urlType)
        let URL_created = "\(BASE_URL)user/\(mobileNumber!)\(User_OTP_Verify_Api)?otp=\(otpPinView.getPin())"
        let headerValue = ["Content-Type":"application/json"]
        
        let paramValues = ["":""] as [String : Any]
        
        ServiceAPI.sharedInstance.callApi(url:URL_created, param_Values:paramValues, header_Value:headerValue, requestMethod:"get", view:self, callbackSuccess: { (Home_Data_JSON) -> Void in
            
            activityIndicator.stopAnimating() // On response stop animating
            activityIndicator.removeFromSuperview() // remove the view
            
            self.view.isUserInteractionEnabled = true
            
            let JSON_Data = JSON(Home_Data_JSON!)
            let statusCode = JSON_Data["code"].intValue
            let messageString = JSON_Data["message"].stringValue
            
            if statusCode == Common_Response_Code {
                self.view.showToast(toastMessage: "Enter new password", duration: 1.5)
                self.SaveNewPasswordView()
            }
            else if messageString != "" {
                self.view.showToast(toastMessage: messageString, duration: 1.5)
            }
            else{
                self.view.showToast(toastMessage: AlertMessage.Some_Thing_Wrong, duration: 1.5)
            }
            
            
        }, callbackFaliure: { (error:String, message:String) -> Void in
            
            activityIndicator.stopAnimating() // On response stop animating
            activityIndicator.removeFromSuperview() // remove the view
            
            self.view.isUserInteractionEnabled = true
            self.view.showToast(toastMessage: message, duration: 1.5)
        })
    }
    
    //TODO :- Func to set save new password screen visible
    func SaveNewPasswordView(){
        navBarView?.navTitleLabel.text = "New Password"
        loginPasswordView.isHidden = true
        otpLoginView.isHidden = true
        setPasswordView.isHidden = false
    }
    
    //TODO :- Func to set new password btn action
    @IBAction func setPasswordBtnAction(_ sender: Any) {
        guard setPasswordTextField.text != "" else {
            Utility.showAlertView(AlertMessage.Enter_Your_Password, title: AlertMessage.Main_Alert_Message)
            return
        }
        guard (setPasswordTextField.text?.count)! > 5 else {
            Utility.showAlertView(AlertMessage.Set_Password_Length, title: AlertMessage.Main_Alert_Message)
            return
        }
        
        callSetNewPasswordApi()
    }
    
    //TODO :- Func to set new password
    func callSetNewPasswordApi(){
        let activityIndicator = UIActivityIndicatorView(style:.whiteLarge) // Create the activity indicator
        view.addSubview(activityIndicator) // add it as a  subview
        activityIndicator.center = CGPoint(x: screenWidth*0.5, y: view.frame.size.height*0.5) // put in the middle
        activityIndicator.color = customColor.loaderColor
        activityIndicator.startAnimating() // Start animating
        
        self.view.isUserInteractionEnabled = false
    
        let BASE_URL = Utility.returnBaseURL(liveURL: urlType)
        let URL_created = "\(BASE_URL)\(Set_User_New_Password)?otp=\(otpPinView.getPin())&username=\(mobileNumber!)&newPassword=\(setPasswordTextField.text!)"
        let headerValue = ["Content-Type":"application/json"]
        
        let paramValues = ["":""] as [String : Any]
        
        ServiceAPI.sharedInstance.callApi(url:URL_created, param_Values:paramValues, header_Value:headerValue, requestMethod:"get", view:self, callbackSuccess: { (Home_Data_JSON) -> Void in
            
            activityIndicator.stopAnimating() // On response stop animating
            activityIndicator.removeFromSuperview() // remove the view
            
            self.view.isUserInteractionEnabled = true
            
            let JSON_Data = JSON(Home_Data_JSON!)
            let statusCode = JSON_Data["code"].intValue
            let messageString = JSON_Data["message"].stringValue
            
            if statusCode == Common_Response_Code {
                self.view.showToast(toastMessage: "Enter your new changed password", duration: 1.5)
                self.navBarView?.navTitleLabel.text = "Password"
                self.loginPasswordView.isHidden = false
                self.otpLoginView.isHidden = true
                self.setPasswordView.isHidden = true
            }
            else if messageString != "" {
                self.view.showToast(toastMessage: messageString, duration: 1.5)
            }
            else{
                self.view.showToast(toastMessage: AlertMessage.Some_Thing_Wrong, duration: 1.5)
            }
            
            
        }, callbackFaliure: { (error:String, message:String) -> Void in
            
            activityIndicator.stopAnimating() // On response stop animating
            activityIndicator.removeFromSuperview() // remove the view
            
            self.view.isUserInteractionEnabled = true
            self.view.showToast(toastMessage: message, duration: 1.5)
        })
    }

}
