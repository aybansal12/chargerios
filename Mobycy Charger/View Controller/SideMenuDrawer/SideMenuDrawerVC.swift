//
//  SideMenuDrawerVC.swift
//  Mobycy Charger
//
//  Created by sarvesh singh on 1/14/20.
//  Copyright © 2020 Zypp. All rights reserved.
//

import UIKit
import SDWebImage

//protocol SlideMenuDelegate {
//    func slideMenuItemSelectedAtIndex(_ index : Int32)
//}

class SideMenuDrawerVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var btnCloseMenuOverlay : UIButton!
//    
//    /**
//     *  Menu button which was tapped to display the menu
//     */
//    var btnMenu : UIButton!
//    var delegate : SlideMenuDelegate?
    
    var navController = UINavigationController()
    let defaults = UserDefaults.standard
    
    var delegate: HomeControllerDelegate?
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    
    @IBOutlet weak var bottomDataView: UIView!
    @IBOutlet weak var appVersionLbl: UILabel!
    
    @IBOutlet weak var drawerTableView: UITableView!
    
    var menuTitleData = [""]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        drawerTableView.delegate = self
        drawerTableView.dataSource = self
        drawerTableView.separatorStyle = .none
        drawerTableView.rowHeight = UITableView.automaticDimension;
        
        let drawerList = UINib(nibName : "DrawerListTableViewCell" , bundle:nil)
        drawerTableView.register(drawerList, forCellReuseIdentifier: "DrawerListTableCell")
        
        menuTitleData = ["My Attendance","Logout"]
        
        getPageData()
        drawerDesignView()
    }
    
    //TODO :- Func to get page data
    func getPageData(){
        var userName = ""
        if (defaults.object(forKey:UserDefaults.UserDefaultsKeys.userName.rawValue) != nil) {
            userName = defaults.getUserName()
        }
        else{
            userName = ""
        }
        
        if userName == "" {
            userNameLbl.text = "Hi Guest!"
        }
        else{
            userNameLbl.text = userName
        }
        
        var userProfileID = ""
        if (defaults.object(forKey:UserDefaults.UserDefaultsKeys.userProfileImage.rawValue) != nil) {
            userProfileID = defaults.getUserProfileImage()
        }
        else{
            userProfileID = ""
        }
        
        if userProfileID != "" {
            let BASE_URL = Utility.returnBaseURL(liveURL: urlType)
            let imageUrl = BASE_URL + "media/file/" + "\(userProfileID)"
            userImageView.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "profilepic"))
//            userImageView?.sd_setImage(with: imageUrl) { (image, error, cache, urls) in
//                if (error != nil) {
//                    //Failure code here
//                    cell.imageView.image = UIImage(named: "profilepic")
//                } else {
//                    //Success code here
//                    cell.imageView.image = image
//                }
//            }
            
//            userImageView.image = UIImage(url: URL(string: imageUrl))
        }
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        appVersionLbl.text = "Version: " + appVersion!
    }
    
    //TODO :- func to set page design layout
    func drawerDesignView(){
        
        userImageView.layer.cornerRadius = userImageView.frame.height/2
        userImageView.layer.masksToBounds = true
        
        userNameLbl.textColor = customColor.mainHeaderColor
        userNameLbl.font = UIFont(name: TextFontStyle.fontSemiBold, size: CGFloat(FontSize.headingFont16))
        
        appVersionLbl.textColor = customColor.textSubGrayColor
        appVersionLbl.font = UIFont(name: TextFontStyle.fontRegular, size: CGFloat(FontSize.headingFont10))
    }
    
    //TODO :- Close menn action
    @IBAction func onCloseMenuClick(_ button:UIButton!) {
//        btnMenu.tag = 0
//
//        if (self.delegate != nil) {
//            var index = Int32(button.tag)
//            if(button == self.btnCloseMenuOverlay){
//                index = -1
//            }
//            delegate?.slideMenuItemSelectedAtIndex(index)
//        }
//
//        UIView.animate(withDuration: 0.3, animations: { () -> Void in
//            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
//            self.view.layoutIfNeeded()
//            self.view.backgroundColor = UIColor.clear
//        }, completion: { (finished) -> Void in
//            self.view.removeFromSuperview()
//            self.removeFromParent()
//        })
        
        delegate?.handleMenuToggle(forMenuOption: nil)
    }
    
    //TODO :- table view functions
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if menuTitleData.count > 0 {
            return menuTitleData.count
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = drawerTableView.dequeueReusableCell(withIdentifier: "DrawerListTableCell", for: indexPath as IndexPath) as! DrawerListTableViewCell
        
        cell.selectionStyle = .none
        
        cell.drawerOptionLbl.text = menuTitleData[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cellIndex = indexPath.row
        
        switch cellIndex {
        case 0:
            print("Attendance")
        case 1:
            
            Utility.showAlertControllerAction(message: "Are you sure you want to logout?", title: "Logout!", onView: self, andFirstButtonTitle: "NOT NOW", andSecondButtonTitle:"YES", andFirstAction: nil, andSecondAction: #selector(SideMenuDrawerVC.logoutUser))
            
        default:
            print("nothing")
        }
    }
    
    // TODO :- logout function
    @objc func logoutUser(){
        
        self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.userID.rawValue)
        self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.userName.rawValue)
        self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.userMobileNo.rawValue)
        self.defaults.removeObject(forKey:UserDefaults.UserDefaultsKeys.isLoggedIn.rawValue)
        
        self.defaults.setUserLogIn(value: "false")
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //appDelegate.window?.rootViewController = LoginScreenVC()
        
        self.navController = UINavigationController(rootViewController: LoginScreenVC())
        self.navController.setNavigationBarHidden(true, animated: false)
        appDelegate.window?.rootViewController = self.navController
        
//        let nextVC = LoginScreenVC()
//        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
}
