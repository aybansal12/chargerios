//
//  ContainerController.swift
//  Mobycy Charger
//
//  Created by sarvesh singh on 1/14/20.
//  Copyright © 2020 Zypp. All rights reserved.
//

import UIKit

class ContainerController: UIViewController {
    
    // MARK: - Properties
    
    var menuController: SideMenuDrawerVC!
    var centerController: UIViewController!
    var isExpanded = false
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureHomeController()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide
    }
    
    override var prefersStatusBarHidden: Bool {
        return isExpanded
    }
    
    // MARK: - Handlers
    
    func configureHomeController() {
        let homeController = HomeScreenVC()
        homeController.delegate = self
        centerController = UINavigationController(rootViewController: homeController)
        
        self.view.addSubview(centerController.view)
        self.addChild(centerController)
        centerController.didMove(toParent: self)
        centerController.view.layoutIfNeeded()
    }
    
    func configureMenuController() {
        if menuController == nil {
            menuController = SideMenuDrawerVC()
            menuController.delegate = self
            menuController.view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height )
            view.insertSubview(menuController.view, at: 0)
            addChild(menuController)
            menuController.didMove(toParent: self)
        }
    }
    
    func animatePanel(shouldExpand: Bool, menuOption: MenuOption?) {
        
        if shouldExpand {
            // show menu
//            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
//                self.centerController.view.frame.origin.x = self.centerController.view.frame.width - 80
//            }, completion: nil)
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.centerController.view.frame.origin.x = self.centerController.view.frame.width - 70
            }, completion:nil)
            
        } else {
            // hide menu
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.centerController.view.frame.origin.x = 0
                
            }) { (_) in
                guard let menuOption = menuOption else { return }
                self.didSelectMenuOption(menuOption: menuOption)
            }
            
//            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
//                self.centerController.view.frame.origin.x = 0
//            }) { (_) in
//                guard let menuOption = menuOption else { return }
//                self.didSelectMenuOption(menuOption: menuOption)
//            }
        }
        
        animateStatusBar()
    }
    
    func didSelectMenuOption(menuOption: MenuOption) {
        switch menuOption {
        case .Profile:
            print("Show profile")
        case .Inbox:
            print("Show Inbox")
        case .Notifications:
            print("Show Notifications")
        }
    }
    
    func animateStatusBar() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.setNeedsStatusBarAppearanceUpdate()
        }, completion: nil)
    }
}

extension ContainerController: HomeControllerDelegate {
    func handleMenuToggle(forMenuOption menuOption: MenuOption?) {
        if !isExpanded {
            configureMenuController()
        }
        
        isExpanded = !isExpanded
        animatePanel(shouldExpand: isExpanded, menuOption: menuOption)
    }
}

