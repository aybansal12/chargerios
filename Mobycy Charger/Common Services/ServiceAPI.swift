//
//  ServiceAPI.swift
//  Mobycy Charger
//
//  Created by sarvesh singh on 1/17/20.
//  Copyright © 2020 Zypp. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ServiceAPI {
    
    static let sharedInstance:ServiceAPI = ServiceAPI()
    let center = NotificationCenter.default
    let AlamoFireManager : Alamofire.SessionManager?
    
    let defaults = UserDefaults.standard
    let delegate = UIApplication.shared.delegate as! AppDelegate
    
    //MARK:-INIT For Configure
    init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 10000 // seconds
        configuration.timeoutIntervalForResource = 10000
        self.AlamoFireManager = Alamofire.SessionManager(configuration: configuration)
        AlamoFireManager?.session.configuration.timeoutIntervalForRequest = 10000
    }
    
    /*******************************    Customer   Controller  ***********************************/
    
    // MARK:- User Registration
//    func userRegistration(custID:String, nameValue:String, emailID:String, mobile_No:String, refrral_Code:String, device_Token:String, version_Name:String, ip_Value:String, location:String, resend_OTP:Bool, updateName_Email:Bool, callbackSuccess:@escaping (_ home_Data_JSON:JSON?) -> Void
//        , callbackFaliure:@escaping ( _ errorTitle:String,_ errorMessage:String) -> Void){
//
//        var Response_JSON = JSON()
//
//        let URL_created = "\(BASE_URL)\(User_Registration)"
//
//        let device_id = UIDevice.current.identifierForVendor!.uuidString
//
//        let parameters   =  [
//            "appVer" : version_Name,
//            "customerId": custID,
//            "dvcTkn" : device_Token,
//            "dvcType" : "ios",
//            "dvcUniqueId" : device_id,
//            "email" : emailID,
//            "ip" : ip_Value,
//            "location" : location,
//            "mobile" : mobile_No,
//            "name" : nameValue,
//            "reSentOtp" : resend_OTP,
//            "updateNameOrEmail" : updateName_Email
//            ] as [String : Any]
//
//        let headers = ["Content-Type":"application/json"]
//
//        if Reachability.isConnectedToNetwork(){
//            AlamoFireManager?.request(URL_created,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON{(response:DataResponse<Any>) in
//
//                switch(response.result) {
//                case .success(let data):
//
//                    Response_JSON = JSON(data)
//
//                    let Response_JSON_Code = Response_JSON["responseCode"]
//
//                    if(Response_JSON_Code == 403){
//                        if Response_JSON["message"].exists(){
//                            let messageForUpdate = Response_JSON["message"].stringValue
//                            if messageForUpdate != "" {
//                                callbackFaliure("Request Failed", messageForUpdate)
//                            }
//                            else{
//                                callbackFaliure("Request Failed",AlertMessage.Failed_To_Connect_Server)
//                            }
//                        }
//                        else{
//                            callbackFaliure("Request Failed",AlertMessage.Failed_To_Connect_Server)
//                        }
//                    }
//                    else{
//                        callbackSuccess(Response_JSON)
//                    }
//
//                case .failure( _):
//                    callbackFaliure("Request Failed",AlertMessage.Failed_To_Connect_Server)
//                }
//            }
//
//        }else{
//            let message = AlertMessage.No_Internet
//            callbackFaliure("No Internet",message)
//        }
//    }
    
    /************************************ Dynamic Api   *******************************/
    
    func callApi(url:String, param_Values:[String:Any]? = nil, header_Value:[String:Any]! = nil, requestMethod:String, view:UIViewController, callbackSuccess:@escaping (_ home_Data_JSON:JSON?) -> Void
        , callbackFaliure:@escaping ( _ errorTitle:String,_ errorMessage:String) -> Void){
        
        var Response_JSON = JSON()
        
        var request_Method:HTTPMethod = HTTPMethod.get
        if requestMethod == "post" {
            request_Method = HTTPMethod.post
        }
        else{
            request_Method = HTTPMethod.get
        }
        
        if Reachability.isConnectedToNetwork(){
            
            let Auth_header:HTTPHeaders!
            if(header_Value != nil){
                Auth_header  = (header_Value as! HTTPHeaders)
            }else{
                Auth_header = nil
            }
            
            AlamoFireManager?.request(url, method:request_Method, parameters: param_Values, encoding: URLEncoding.default, headers: Auth_header).responseJSON{(response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(let data):
                    
                    Response_JSON = JSON(data)
        
                    callbackSuccess(Response_JSON)
                    
                case .failure( _):
                    callbackFaliure("Request Failed",AlertMessage.Failed_To_Connect_Server)
                }
            }
            
        }else{
            let message = AlertMessage.No_Internet
            callbackFaliure("No Internet",message)
        }
    }
    
    func callJSONEncodnigApi(url:String, param_Values:[String:Any]? = nil, header_Value:[String:Any]! = nil, requestMethod:String, view:UIViewController, callbackSuccess:@escaping (_ home_Data_JSON:JSON?) -> Void
        , callbackFaliure:@escaping ( _ errorTitle:String,_ errorMessage:String) -> Void){
        
        var Response_JSON = JSON()
        
        var request_Method:HTTPMethod = HTTPMethod.get
        if requestMethod == "post" {
            request_Method = HTTPMethod.post
        }
        else{
            request_Method = HTTPMethod.get
        }
        
        if Reachability.isConnectedToNetwork(){
            
            let Auth_header:HTTPHeaders!
            if(header_Value != nil){
                Auth_header  = (header_Value as! HTTPHeaders)
            }else{
                Auth_header = nil
            }
            
            AlamoFireManager?.request(url, method:request_Method, parameters: param_Values, encoding: JSONEncoding.default, headers: Auth_header).responseJSON{(response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(let data):
                    
                    Response_JSON = JSON(data)
                    
                    callbackSuccess(Response_JSON)
                    
                case .failure( _):
                    callbackFaliure("Request Failed",AlertMessage.Failed_To_Connect_Server)
                }
            }
            
        }else{
            let message = AlertMessage.No_Internet
            callbackFaliure("No Internet",message)
        }
    }
    
    func callJSONEncodnigApiWithOutController(url:String, param_Values:[String:Any]? = nil, header_Value:[String:Any]! = nil, requestMethod:String, callbackSuccess:@escaping (_ home_Data_JSON:JSON?) -> Void
        , callbackFaliure:@escaping ( _ errorTitle:String,_ errorMessage:String) -> Void){
        
        var Response_JSON = JSON()
        
        var request_Method:HTTPMethod = HTTPMethod.get
        if requestMethod == "post" {
            request_Method = HTTPMethod.post
        }
        else{
            request_Method = HTTPMethod.get
        }
        
        if Reachability.isConnectedToNetwork(){
            
            let Auth_header:HTTPHeaders!
            if(header_Value != nil){
                Auth_header  = (header_Value as! HTTPHeaders)
            }else{
                Auth_header = nil
            }
            
            AlamoFireManager?.request(url, method:request_Method, parameters: param_Values, encoding: JSONEncoding.default, headers: Auth_header).responseJSON{(response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(let data):
                    
                    Response_JSON = JSON(data)
                    
                    let Response_JSON_Code = Response_JSON["responseCode"]
                    
                    if(Response_JSON_Code == 403){
                        if Response_JSON["message"].exists(){
                            let messageForUpdate = Response_JSON["message"].stringValue
                            if messageForUpdate != "" {
                                callbackFaliure("Request Failed", messageForUpdate)
                            }
                            else{
                                callbackFaliure("Request Failed",AlertMessage.Failed_To_Connect_Server)
                            }
                        }
                        else{
                            callbackFaliure("Request Failed",AlertMessage.Failed_To_Connect_Server)
                        }
                    }
                    else if Response_JSON_Code == 200 || Response_JSON_Code == 401 || Response_JSON_Code == 201 || Response_JSON_Code == 202 {
                        callbackSuccess(Response_JSON)
                    }
                    else{
                        callbackFaliure("Something went wrong",AlertMessage.Some_Thing_Wrong)
                    }
                    
                case .failure( _):
                    callbackFaliure("Request Failed",AlertMessage.Failed_To_Connect_Server)
                }
            }
            
        }else{
            let message = AlertMessage.No_Internet
            callbackFaliure("No Internet",message)
        }
    }
    
    /************************************ End Dynamic Api  *******************************/
}
