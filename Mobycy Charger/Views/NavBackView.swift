//
//  NavBackView.swift
//  Mobycy Charger
//
//  Created by sarvesh singh on 1/14/20.
//  Copyright © 2020 Zypp. All rights reserved.
//

import UIKit

protocol navBakButtonWithPressed {
    func didPressButton(button:UIButton)
}


class NavBackView: UIView {

    var delegate:navBakButtonWithPressed!
    
    @IBOutlet var mainView: UIView!
    @IBOutlet var navBackButton: UIButton!
    @IBOutlet var navTitleLabel: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mainView.backgroundColor = customColor.mainHeaderColor
        
        navTitleLabel.textColor = customColor.completeWhiteColor
        navTitleLabel.font = UIFont(name: TextFontStyle.fontBold, size: CGFloat(FontSize.headingFont16))
    }
    
    @IBAction func navBackButton(_ sender: UIButton) {
        delegate.didPressButton(button: navBackButton)
    }

}
