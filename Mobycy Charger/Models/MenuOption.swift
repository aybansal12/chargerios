//
//  MenuOption.swift
//  Mobycy Charger
//
//  Created by sarvesh singh on 1/14/20.
//  Copyright © 2020 Zypp. All rights reserved.
//

import UIKit

enum MenuOption: Int, CustomStringConvertible {
    
    case Profile
    case Inbox
    case Notifications
    
    var description: String {
        switch self {
        case .Profile: return "Profile"
        case .Inbox: return "Inbox"
        case .Notifications: return "Notifications"
        }
    }
    
    var image: UIImage {
        switch self {
        case .Profile: return UIImage(named: "") ?? UIImage()
        case .Inbox: return UIImage(named: "") ?? UIImage()
        case .Notifications: return UIImage(named: "") ?? UIImage()
        }
    }
}
